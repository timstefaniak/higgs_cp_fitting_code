import pymc3 as pm
print('Running on PyMC3 v{}'.format(pm.__version__))
import numpy as np
import theano
from theano.compile.ops import as_op
import theano.tensor as tt
from scipy.optimize import  approx_fprime

import HiggsSignals.HS as HS
from settings import *


print("Initialize HiggsSignals...")
#HS.initialize(1,0,"LHC13_Apr2020")
HS.initialize(1,0,"LHC13_Apr2020_wo_ttH")
# HS.initialize(1,0,"tHttH_prospects")
# HS.initialize(1,0,"LHC13_Jan2020_wo_ttH")
#HS.initialize(1,0,"LHC13_Jan2020_wo_ZHbb")

print("Setting up some HiggsSignals options...")
HS.setup_output_lvl(0)


#---------- loop functions ----------------------------------#

def f_func(t):
	if(t<=1):
		return np.arcsin(np.sqrt(t))**2
	else:
		return -1./4.*(np.log((1+np.sqrt(1-1/t))/(1-np.sqrt(1-1/t)))-1j*np.pi)**2

def Hhalf_func(t):
	return ((t-1)*f_func(t)+t)/t**2

def Hone_func(t):
	return (3*(2*t-1)*f_func(t)+3*t+2*t**2)/(2*t**2)

def Ahalf_func(t):
	return f_func(t)/t

#---------- Functions to obtain rate predictions ------------#

# def mu_hgaga(kteven,kgaeven,ktodd,kgaodd,kV):
#     return (256.*(kteven + kgaeven)**2.+576.*(ktodd + kgaodd)**2. \
#         - 2016.*(kteven+kgaeven)*kV +3969*kV**2.)/2209.
mH = 125.0
mt = 173.34
ms = 0.105
mb = 4.20
mmu = 0.1057
mc = 1.27
mtau = 1.777
mW = 80.385

kbeven = 1.
ktaueven = 1.
kbodd = 0.
ktauodd = 0.


def tau(m):
    return mH**2./(4.*m**2.)

taut = tau(mt)
tauw = tau(mW)
taub = tau(mb)
tautau = tau(mtau)

def mu_hgaga(kteven,ktodd,kV):
    return mu_hgaga_full(kteven,0.,ktodd,0.,kV)

def mu_hgaga_full(kteven,kgaeven,ktodd,kgaodd,kV):
    # Higher dim Hgammagamma operators currently not implemented!
    G_CPeven = np.abs(4./3.*kteven*Hhalf_func(taut) + \
	                  1./3.*kbeven*Hhalf_func(taub) + \
	                  1.*ktaueven*Hhalf_func(tautau) - \
	                  1.*kV*Hone_func(tauw) )**2
    G_CPodd = np.abs(4./3.*ktodd*Ahalf_func(taut) + \
	                 1./3.*kbodd*Ahalf_func(taub) + \
	                    1.*ktauodd*Ahalf_func(tautau))**2
                        # + (cB*cWsq+cW*sWsq)/(np.sqrt(2)*e**2))**2 / \
    G_SM = np.abs(4./3.*Hhalf_func(taut) + \
	                  1./3.*Hhalf_func(taub) + \
	                  1.*Hhalf_func(tautau) - \
	                  1.*Hone_func(tauw) )**2
    return (G_CPeven + G_CPodd)/G_SM

# def mu_hgaga_simplified(kteven,kgaeven,ktodd,kgaodd,kV):
#     return (   abs(4/3.*kteven*Hhalf_func(taut) - kV*Hone_func(tauw))**2 \
#             +  abs(4/3.*ktodd*Ahalf_func(taut))**2 ) \
#            / abs(4/3.*Hhalf_func(taut) - Hone_func(tauw))**2

def mu_ggh_incl(kteven, ktodd):
    G_CPeven = np.abs(kteven*Hhalf_func(taut) + \
                      kbeven*Hhalf_func(taub))**2.
    G_CPodd = np.abs(ktodd*Ahalf_func(taut) + \
                     kbodd*Ahalf_func(taub))**2.
                        # np.sqrt(2)*cG/(gs**2) )**2 / \
    G_SM = np.abs(Hhalf_func(taut) + Hhalf_func(taub))**2.
    return (G_CPeven +  G_CPodd)/G_SM

# def mu_ggh_incl_simplified(kteven,kgeven,ktodd,kgodd):
#     return (kteven+kgeven)**2. + 9./4.*(ktodd+kgodd)**2.

def sigma_ggZH_incl(kteven,ktodd,kV):
    #Not really needed. Using HB-internal function via effC approximation
    return 24.4*kteven**2+27.3*ktodd**2.+132.*kV**2.-102.2*kteven*kV

def mu_ggZH_incl(kteven,ktodd,kV):
    return sigma_ggZH_incl(kteven,ktodd,kV)/sigma_ggZH_incl(1.0,0.0,1.0)

def sigma_ggZH_bin1(kteven,ktodd,kV):
    return 4.7*kteven**2+5.7*ktodd**2.+46.1*kV**2.-27.3*kteven*kV

def sigma_ggZH_bin2(kteven,ktodd,kV):
    return 8.5*kteven**2+9.7*ktodd**2.+44.0*kV**2.-36.5*kteven*kV

def sigma_ggZH_bin3(kteven,ktodd,kV):
    return 9.8*kteven**2+10.2*ktodd**2.+25.6*kV**2.-29.7*kteven*kV

def sigma_ggZH_binsum(kteven,ktodd,kV):
    return sigma_ggZH_bin1(kteven,ktodd,kV)+ \
        sigma_ggZH_bin2(kteven,ktodd,kV) + \
        sigma_ggZH_bin3(kteven,ktodd,kV)

def sigma_ggh2j_bin1(kteven,ktodd):
	return (89.4*kteven**2.+0.1*kteven*ktodd+194.8*ktodd**2.)/ \
	       (148.8*kteven**2. + 0.9*kteven*ktodd+328.5*ktodd**2.)

def sigma_ggh2j_bin2(kteven,ktodd):
	return (59.4*kteven**2.+0.8*kteven*ktodd+133.7*ktodd**2.)/ \
	       (148.8*kteven**2. + 0.9*kteven*ktodd+328.5*ktodd**2.)

def r_ggh2j_bin1(kteven,ktodd):
	return sigma_ggh2j_bin1(kteven,ktodd)/sigma_ggh2j_bin1(1.,0.)

def r_ggh2j_bin2(kteven,ktodd):
	return sigma_ggh2j_bin2(kteven,ktodd)/sigma_ggh2j_bin2(1.,0.)

def sigma_tH(kteven,ktodd,kV):
    return 200.6*kteven**2.+233.8*kV**2.-373.2*kV*kteven+60.9*ktodd**2.

def sigma_ttH(kteven,ktodd):
    return 371.6*kteven**2.+155.8*ktodd**2.

def r_tH(kteven,ktodd,kV):
    return sigma_tH(kteven,ktodd,kV)/sigma_tH(1.,0.,1.)

def r_ttH(kteven,ktodd):
    return sigma_ttH(kteven,ktodd)/sigma_ttH(1.,0.)

def r_ggZH(bin,kteven,ktodd,kV):
    factor = 1.
    if (bin == 1):
        factor = sigma_ggZH_bin1(kteven,ktodd,kV)/sigma_ggZH_bin1(1.0,0.0,1.0)
    elif(bin == 2):
        factor = sigma_ggZH_bin2(kteven,ktodd,kV)/sigma_ggZH_bin2(1.0,0.0,1.0)
    elif(bin == 3):
        factor = sigma_ggZH_bin3(kteven,ktodd,kV)/sigma_ggZH_bin3(1.0,0.0,1.0)
    else:
        print("Error in function r_ggZH! Wrong bin specifier!")
    # Use binsum as inclusive cross section.
    # return factor * sigma_ggZH_binsum(1.0,0.0,1.0)/sigma_ggZH_binsum(kteven,ktodd,kV)
    # Use HB-internal function as inclusive cross section.
    return factor * HS.sigma_ggzh_fct(mH, 1.0, 1.0, 0.0, 1.0, 0.0)/HS.sigma_ggzh_fct(mH, kV, kteven, ktodd, 1.0, 0.0)

def HSrun(theta):
    global derive_loopcouplings
    global free_loopcouplings
    kV,kteven,ktodd,kgeven,kggZH,kgaeven,kgaodd,BRNP = theta

    if derive_loopcouplings:
        kg = np.sqrt(mu_ggh_incl(kteven,ktodd))
        kga = np.sqrt(mu_hgaga(kteven,ktodd,kV))
    elif free_loopcouplings:
        kg = kgeven # use kgeven as full free kappa_g scale factor
        kga = kgaeven # use kgeven as full free kappa_g scale factor
    elif free_kappagamma:
        kg = np.sqrt(mu_ggh_incl(kteven,ktodd))
        kga = kgaeven
    else:
        kg = 1.
        kga = 1.

    # print(kg, kga)

    HS.input_effc(mH,kteven,ktodd,kV,kg,kga,BRNP)

    if free_ggZH:
        HS.process_input()
        HS.input_ppzh(kV,kggZH)
        HS.input_ggzh(kggZH)
    if account_for_kinematics:
        HS.assign_modfactor(20200063,[[1.,r_ggZH(1,kteven,ktodd,kV)]],2) # ATLAS, ZH,H->bb, pT: [75,150] GeV
        HS.assign_modfactor(20200064,[[1.,r_ggZH(2,kteven,ktodd,kV)]],2) # ATLAS, ZH,H->bb, pT: [150,250] GeV
        HS.assign_modfactor(20200065,[[1.,r_ggZH(3,kteven,ktodd,kV)]],2) # ATLAS, ZH,H->bb, pT: [150,250] GeV

	# relevant only for model 1 and model 2 (with derived kappa_g)

    if derive_loopcouplings or free_kappagamma:
        HS.assign_modfactor(20190293,[[r_ggh2j_bin1(kteven,ktodd),1.,1.,1.,1.,1.,1.,1.]],8) # ATLAS, pp->H+2jets, H->gaga, abs(DeltaPhi_jj) in [Pi/2,Pi]
        HS.assign_modfactor(20190296,[[r_ggh2j_bin1(kteven,ktodd),1.,1.,1.,1.,1.,1.,1.]],8) # ATLAS, pp->H+2jets, H->gaga, abs(DeltaPhi_jj) in [Pi/2,Pi]
        HS.assign_modfactor(20190294,[[r_ggh2j_bin2(kteven,ktodd),1.,1.,1.,1.,1.,1.,1.]],8) # ATLAS, pp->H+2jets, H->gaga, abs(DeltaPhi_jj) in [0,Pi/2]
        HS.assign_modfactor(20190295,[[r_ggh2j_bin2(kteven,ktodd),1.,1.,1.,1.,1.,1.,1.]],8) # ATLAS, pp->H+2jets, H->gaga, abs(DeltaPhi_jj) in [0,Pi/2]

    chi2peak = HS.run()[0]
    chi2run1 = HS.run_lhcrun1()[0]
    chi2stxs = HS.run_stxs()[0]
    chi2tot = chi2peak + chi2run1 + chi2stxs

    Gtot,GtotSM = HS.get_totalwidth()
    # print(Gtot,GtotSM)
    if GtotSM > 0:
        kH = Gtot/GtotSM

    mu_all_gaga = HS.rate_fct(["1.1  ","2.1  ","3.1  ","4.1  ","5.1  "],5)
    mu_all_ZZ = HS.rate_fct(["1.3  ","2.3  ","3.3  ","4.3  ","5.3  "],5)
    mu_all_bb = HS.rate_fct(["1.5  ","2.5  ","3.5  ","4.5  ","5.5  "],5)
    mu_ppZH_bb = HS.rate_fct(["4.5  "],1)
    mu_qqZH_bb = HS.rate_fct(["10.5 "],1)
    mu_ggZH_bb = HS.rate_fct(["11.5 "],1)
    mu_ggH = HS.rate_fct(["6.0  "],1)
    mu_Hgaga = HS.rate_fct(["0.1  "],1)
    mu_ppZH = HS.rate_fct(["4.0  "],1)
    mu_qqZH = HS.rate_fct(["10.0 "],1)
    mu_ggZH = HS.rate_fct(["11.0 "],1)
    mu_tH = HS.rate_fct(["8.0  "],1)
    mu_ttH = HS.rate_fct(["5.0  "],1)
    mu_tWH = HS.rate_fct(["12.0 "],1)
    mu_ttHtWH = HS.rate_fct(["5.0  ","12.0 "],2)
    mu_tHttH = HS.rate_fct(["5.0  ","8.0  "],2)
    mu_topH = HS.rate_fct(["5.0  ","8.0  ", "9.0  ", "12.0 "],4)
    mu_ttH_gaga = HS.rate_fct(["5.1  "],1)

    results = [chi2tot,chi2peak,chi2run1,chi2stxs,kH,\
               mu_all_gaga,mu_all_ZZ,mu_all_bb,\
               mu_ppZH_bb,mu_qqZH_bb,mu_ggZH_bb,\
               mu_ggH,mu_Hgaga,mu_ppZH,mu_qqZH,mu_ggZH,\
               mu_tH, mu_ttH, mu_tWH, mu_ttHtWH, mu_tHttH, mu_topH, mu_tH/mu_ttHtWH]

    # print(kV,kteven,ktodd,chi2tot)
    # print("kV,kteven,ktodd,chi2tot,r_all_gaga = ",kV, kteven, ktodd, chi2tot, r_all_gaga)
    return results

# def likelihood(kV,kteven,ktodd,kgeven,kggZH,kgaeven,kgaodd,BRNP):
#     hslogp = -1./2.*HSrun(kV,kteven,ktodd,kgeven,kggZH,kgaeven,kgaodd,BRNP)[0]
#     return hslogp

# define a theano Op for our likelihood function
class LogLike(tt.Op):

    """
    Specify what type of object will be passed and returned to the Op when it is
    called. In our case we will be passing it a vector of values (the parameters
    that define our model) and returning a single "scalar" value (the
    log-likelihood)
    """
    itypes = [tt.dvector] # expects a vector of parameter values when called
    otypes = [tt.dscalar] # outputs a single scalar value (the log likelihood)

    def __init__(self, HSrun):
        """
        Initialise the Op with various things that our log-likelihood function
        requires. Below are the things that are needed in this particular
        example.

        Parameters
        ----------
        HSrun:
            The routine that evaluates the likelihood (calling HiggsSignals)
        """

        # add inputs as class attributes
        self.likelihood = HSrun

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        theta, = inputs  # this will contain my variables

        # call the log-likelihood function
        HSresults = self.likelihood(theta)

        outputs[0][0] = np.array(-1./2.*HSresults[0]) # output the log-likelihood

# define a theano Op for our likelihood function
class LogLikeWithGrad(tt.Op):

    itypes = [tt.dvector] # expects a vector of parameter values when called
    otypes = [tt.dscalar] # outputs a single scalar value (the log likelihood)

    def __init__(self, HSrun):
        """
        Initialise with various things that the function requires. Below
        are the things that are needed in this particular example.

        Parameters
        ----------
        HSrun:
            The routine that evaluates the likelihood (calling HiggsSignals)
        """

        # add inputs as class attributes
        self.likelihood = HSrun

        # initialise the gradient Op (below)
        self.logpgrad = LogLikeGrad(self.likelihood)

    def perform(self, node, inputs, outputs):
        # the method that is used when calling the Op
        theta, = inputs  # this will contain my variables

        # call the log-likelihood function
        HSresults = self.likelihood(theta)

        outputs[0][0] = np.array(-1./2.*HSresults[0]) # output the log-likelihood

    def grad(self, inputs, g):
        # the method that calculates the gradients - it actually returns the
        # vector-Jacobian product - g[0] is a vector of parameter values
        theta, = inputs  # our parameters
        return [g[0]*self.logpgrad(theta)]


class LogLikeGrad(tt.Op):

    """
    This Op will be called with a vector of values and also return a vector of
    values - the gradients in each dimension.
    """
    itypes = [tt.dvector]
    otypes = [tt.dvector]

    def __init__(self, HSrun):
        """
        Initialise with various things that the function requires. Below
        are the things that are needed in this particular example.

        Parameters
        ----------
        HSrun:
            The routine that evaluates the likelihood (calling HiggsSignals)
        """

        # add inputs as class attributes
        self.likelihood = HSrun

    def perform(self, node, inputs, outputs):
        theta, = inputs

        # define version of likelihood function to pass to derivative function
        def lnlike(values):
            HSresults = self.likelihood(values)
            return -1./2.*HSresults[0]

        # calculate gradients
        # grads = gradients(theta, lnlike)
        grads = approx_fprime(theta, lnlike,0.1)
        outputs[0][0] = grads


#logl = LogLike(HSrun)
logl = LogLikeWithGrad(HSrun)

def HiggsCPmodel():

    with pm.Model() as model:
        # Define input variables with flat priors
        kV = pm.Uniform('kV',lower = 0.5, upper = 1.5)
        kteven = pm.Uniform('kteven',lower = -2.0, upper = 2.0)
        ktodd = pm.Uniform('ktodd',lower = -2.0, upper = 2.0)

        # kgeven = pm.Uniform('kgeven',lower = -2.0, upper = 2.0)
        # kggZH = pm.Uniform('kggZH',lower = -2.0, upper = 2.0)
        # kgaeven = pm.Uniform('kgaeven',lower = -2.0, upper = 2.0)
        # kgaodd = pm.Uniform('kgaodd',lower = -2.0, upper = 2.0)
        # BRNP = pm.Uniform('BRNP',lower=0.,upper=1.0)
        kgeven = 0.
        kggZH = 0.
        kgaeven = 0.
        kgaodd = 0.
        BRNP = 0.

        theta = tt.as_tensor_variable([kV,kteven,ktodd,kgeven,kggZH,kgaeven,kgaodd,BRNP])
        like = pm.DensityDist('likelihood',lambda v: logl(v),observed={'v': theta})
    return model


# @as_op(itypes=[tt.dvector],otypes=[tt.dvector])
# def get_HSresults(theta):
#     HSresults = HSrun(theta)
#     return HSresults

#---DEFINE THE PYMC3 MODEL---

# logl = LogLike(HSrun)
