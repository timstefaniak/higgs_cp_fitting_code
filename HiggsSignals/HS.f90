!-----------Collection of wrapper subroutines of HiggsSignals subroutines--------!
subroutine initialize(nHneut,nHplus, Expt_string)
 implicit none
 !--------------------------------------input
 integer,intent(in) :: nHneut,nHplus
 character(LEN=*), intent(in) :: Expt_string
 call initialize_HiggsSignals(nHneut,nHplus,Expt_string)
end subroutine initialize
!--------------------------------------------------------------------------------
subroutine initialize_empty(nHneut,nHplus)
 implicit none
 !--------------------------------------input
 integer,intent(in) :: nHneut,nHplus
 call initialize_HiggsSignals_empty(nHneut,nHplus)
end subroutine initialize_empty
!--------------------------------------------------------------------------------
subroutine setup_output(level)
 implicit none
 integer, intent(in) :: level
 call setup_output_level(level)
end subroutine setup_output
!--------------------------------------------------------------------------------
! subroutine setup_symmerr(symm)
!  implicit none
!  integer, intent(in) :: symm
!  call setup_symmetricerrors(symm)
! end subroutine setup_symmerr
!--------------------------------------------------------------------------------
! subroutine setup_abserr(absol)
!  implicit none
!  integer, intent(in) :: absol
!  call setup_absolute_errors(absol)
! end subroutine setup_abserr
!--------------------------------------------------------------------------------
! subroutine setup_fixedsmweights(fixed)
!  implicit none
!  integer, intent(in) :: fixed
!  call setup_SMweights(fixed)
! end subroutine setup_fixedsmweights
!--------------------------------------------------------------------------------
! subroutine setup_corr(corr_mu, corr_mh)
!  implicit none
!  integer, intent(in) :: corr_mu, corr_mh
!  call setup_correlations(corr_mu, corr_mh)
! end subroutine setup_corr
!--------------------------------------------------------------------------------
! subroutine setup_theory_uncertainties_corr(corr)
!  implicit none
!  integer, intent(in) :: corr
!  call setup_correlated_rate_uncertainties(corr)
! end subroutine setup_theory_uncertainties_corr
!--------------------------------------------------------------------------------
! subroutine setup_thu(on)
!  implicit none
!  integer, intent(in) :: on
!  call setup_thu_observables(on)
! end subroutine setup_thu
!--------------------------------------------------------------------------------
! subroutine setup_anticorrmu(anticorr)
!  implicit none
!  integer, intent(in) :: anticorr
!  call setup_anticorrelations_in_mu(anticorr)
! end subroutine setup_anticorrmu
!--------------------------------------------------------------------------------
! subroutine setup_anticorrmh(anticorr)
!  implicit none
!  integer, intent(in) :: anticorr
!  call setup_anticorrelations_in_mh(anticorr)
! end subroutine setup_anticorrmh
!--------------------------------------------------------------------------------
! subroutine setup_masspdf(pdf)
!  implicit none
!  integer, intent(in) :: pdf
!  call setup_pdf(pdf)
! end subroutine setup_masspdf
! !--------------------------------------------------------------------------------
! subroutine setup_dmh(dmhneut)
!  implicit none
!  double precision, dimension(:),intent(in) :: dmhneut
!  call HiggsSignals_neutral_input_MassUncertainty(dmhneut)
!  end subroutine setup_dmh
!--------------------------------------------------------------------------------
! subroutine setup_CS_BR_uncertainties(dCS, dBR)
!  implicit none
!  double precision, intent(in) :: dCS(5)
!  double precision, intent(in) :: dBR(5)
!  call setup_rate_uncertainties(dCS, dBR)
! end subroutine setup_CS_BR_uncertainties
!--------------------------------------------------------------------------------
subroutine get_smtotalwidth(m,SMGammaTotal)
 implicit none
 double precision, intent(in) :: m
 double precision, intent(out) :: SMGammaTotal
 double precision :: SMGamma_h
 SMGammaTotal=SMGamma_h(m)
end subroutine get_smtotalwidth
!--------------------------------------------------------------------------------
subroutine get_sm_br(m,BR_HWW,BR_HZZ,BR_Hgamgam,BR_Hgg,BR_Htoptop,BR_Hss,BR_Hcc,BR_Hbb,BR_Hmumu,BR_Htautau,BR_HZgam)
 implicit none
 double precision, intent(in) :: m
 double precision, intent(out) :: BR_HWW, BR_HZZ, BR_Hgamgam, BR_Hgg, &
      &BR_Htoptop, BR_Hss, BR_Hcc, BR_Hbb, BR_Hmumu, BR_Htautau, BR_HZgam
 double precision ::              SMBR_HWW, SMBR_HZZ, SMBR_Hgamgam, SMBR_Hgg,&
      &SMBR_Htoptop, SMBR_Hss, SMBR_Hcc, SMBR_Hbb, SMBR_Hmumu, SMBR_Htautau, SMBR_HZgam

 BR_HWW     = SMBR_HWW(m)
 BR_HZZ     = SMBR_HZZ(m)
 BR_Hgamgam = SMBR_Hgamgam(m)
 BR_Hgg     = SMBR_Hgg(m)
 BR_Htoptop = SMBR_Htoptop(m)
 BR_Hss     = SMBR_Hss(m)
 BR_Hcc     = SMBR_Hcc(m)
 BR_Hbb     = SMBR_Hbb(m)
 BR_Hmumu   = SMBR_Hmumu(m)
 BR_Htautau = SMBR_Htautau(m)
 BR_HZgam   = SMBR_HZgam(m)

end subroutine get_sm_br
!--------------------------------------------------------------------------------
function sigma_ggzh_fct(Mh, kV, kteven, ktodd, kbeven, kbodd)
  use theory_XS_SM_functions
  implicit none
  double precision, intent(in) :: Mh, kV, kteven, ktodd, kbeven, kbodd
  double precision :: sigma_ggzh_fct

  sigma_ggzh_fct = ZH_cpmix_nnlo_gg(Mh, 'LHC13', kV, kteven, kbeven, ktodd, kbodd, .True.)
end function sigma_ggzh_fct
! subroutine get_totalWidth_effc12(m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghzga,ghgg,alpha,cG,cB,cW,GammaTotal)
!   implicit none
!
!   double precision, intent(in) :: m
!   double precision, dimension(2), intent(in) :: ghuu,ghdd,ghll
!   double precision, intent(in) :: ghWW,ghZZ,ghgaga,ghzga,ghgg,alpha,cG,cB,cW
!   double precision, intent(out) :: GammaTotal
!
!   double precision :: invbsq
!
!   double precision :: ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,ghjbb_s,ghjbb_p,&
!                     & ghjtt_s,ghjtt_p,ghjmumu_s,ghjmumu_p,ghjtautau_s,ghjtautau_p,&
!                     & ghjWW,ghjZZ,ghjZga,ghjgaga,ghjgg,ghjhiZ,&!ghjggZ,
!                     & BR_hjhihi,BR_hjinvisible,&
!                     & Rcc,Rbb,Rgg,cWsq,sWsq
!   !-HiggsBounds internal functions to obtain SM branching ratios
!   double precision :: SMBR_Htoptop,SMBR_Hss,SMBR_Hcc,SMBR_Hbb,SMBR_Hmumu,SMBR_Htautau,&
!                     & SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg,SMGamma_h
!   ! Definition of Fermion masses
!   double precision :: ms,mc,mb,mt,mmu,mtau,pi
!   ms=0.105d0
!   mc=1.27d0
!   mb=4.20d0
!   mt=173.34d0
!   mmu=105.7d-3
!   mtau=1.777d0
!
!   pi = 3.1415926535897931
!
!   !preliminary definition of Rcc,Rbb and Rgg
!   Rcc = 1.0d0
!   Rbb = 1.0d0
!   Rgg = 1.0d0
!
!   !Weak mxing angle squared
!   cWsq = 0.769
!   sWsq = 0.231
!
!   if(SMGamma_h(m).ge.0.0D0) then
!      ghjss_s = cos(alpha)*ghdd(1)
!      ghjss_p = sin(alpha)*ghdd(2)
!      ghjcc_s =     cos(alpha)*ghuu(1)
!      ghjcc_p = Rcc*sin(alpha)*ghuu(2) !Rcc not correctly defined yet (Rcc=1 so far)
!      ghjbb_s =     cos(alpha)*ghdd(1)
!      ghjbb_p = Rbb*sin(alpha)*ghdd(2) !Rbb not correctly defined yet (Rbb=1 so far)
!      ghjtt_s     = cos(alpha)*ghuu(1)
!      ghjtt_p     = sin(alpha)*ghuu(2)
!      ghjmumu_s   = cos(alpha)*ghll(1)
!      ghjmumu_p   = sin(alpha)*ghll(2)
!      ghjtautau_s = cos(alpha)*ghll(1)
!      ghjtautau_p = sin(alpha)*ghll(2)
!
!      ghjWW = sqrt( (cos(alpha)*ghWW)**2 + (sin(alpha)*cW/(4*pi)**2)**2 * 0.155 )
!      ghjZZ = sqrt( (cos(alpha)*ghZZ)**2 + (sin(alpha)*(cB*sWsq+cW*cWsq)/(4*pi)**2)**2 * 0.074 )
!      ghjZga =cos(alpha)*ghzga ! in principle can be expressed by ghWW, ghtt and ghbb, no sensitive observable yet.
!      ghjgg   = ghgg
! !      ghjggZ  = cos(alpha)*ghZZ
!      ghjhiZ  = 0.0d0
!      ghjgaga = ghgaga
!
!      BR_hjhihi=0.0d0
!      BR_hjinvisible=0.0d0
!
!      !----Calculate the new total decay width:
!      GammaTotal = SMGamma_h(m)*(1 + &
!           & (ghjWW**2.0                                           - 1)*SMBR_HWW(m)    + (ghjZZ**2.0   - 1)*SMBR_HZZ(m) +&
!           & (ghjgg**2.0                                           - 1)*SMBR_Hgg(m)    + (ghjgaga**2.0 - 1)*SMBR_Hgamgam(m)+&
!           & (ghjZga**2.0                                          - 1)*SMBR_HZgam(m)  +&
!           & (ghjbb_s**2.0     + (ghjbb_p**2.0)*invbsq(mb,m)       - 1)*SMBR_Hbb(m)    +&
!           & (ghjtautau_s**2.0 + (ghjtautau_p**2.0)*invbsq(mtau,m) - 1)*SMBR_Htautau(m)+&
!           & (ghjss_s**2.0     + (ghjss_p**2.0)*invbsq(ms,m)       - 1)*SMBR_Hss(m)    +&
!           & (ghjmumu_s**2.0   + (ghjmumu_p**2.0)*invbsq(mmu,m)    - 1)*SMBR_Hmumu(m)  +&
!           & (ghjcc_s**2.0     + (ghjcc_p**2.0)*invbsq(mc,m)       - 1)*SMBR_Hcc(m)    +&
!           & (ghjtt_s**2.0     + (ghjtt_p**2.0)*invbsq(mt,m)       - 1)*SMBR_Htoptop(m) )
!  endif
! end subroutine get_totalWidth_effc12
!--------------------------------------------------------------------------------
! subroutine get_totalWidth_effc8_2(m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghzga,ghgg,alpha,GammaTotal)
!   implicit none
!
!   double precision, intent(in) :: m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghZga,ghgg,alpha
!   double precision, intent(out) :: GammaTotal
!
!   double precision :: ghjss,ghjcc,ghjbb,ghjtt,ghjmumu,ghjtautau,&
!                     & ghjWW,ghjZZ,ghjZga,ghjgaga,ghjgg,ghjhiZ,&!ghjggZ,
!                     & BR_hjhihi,BR_hjinvisible
!   !-HiggsBounds internal functions to obtain SM branching ratios
!   double precision :: SMBR_Htoptop,SMBR_Hss,SMBR_Hcc,SMBR_Hbb,SMBR_Hmumu,SMBR_Htautau,&
!                     & SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg,SMGamma_h
!   ! Definition of Fermion masses
!   double precision :: ms,mc,mb,mt,mmu,mtau
!   ms=0.105d0
!   mc=1.27d0
!   mb=4.20d0
!   mt=173.34d0
!   mmu=105.7d-3
!   mtau=1.777d0
!
!   if(SMGamma_h(m).ge.0.0D0) then
!      ghjss = ghdd
!      ghjcc = ghuu
!      ghjbb = ghdd
!      ghjtt = ghuu
!      ghjmumu = ghll
!      ghjtautau = ghll
!
!      ghjWW = cos(alpha)*ghWW
!      ghjZZ = cos(alpha)*ghZZ
!      ghjZga = 1.0d0
!      ghjgg = ghgg
! !     ghjggZ = cos(alpha)*ghZZ
!      ghjhiZ = 0.0d0
!      ghjgaga = ghgaga
!
!      BR_hjhihi=0.0d0
!      BR_hjinvisible=0.0d0
!
!      !----Calculate the new total decay width:
!      GammaTotal = SMGamma_h(m)*(1 + &
!           & (ghjWW**2.0     - 1)*SMBR_HWW(m)    + (ghjZZ**2.0   - 1)*SMBR_HZZ(m) +&
!           & (ghjgg**2.0     - 1)*SMBR_Hgg(m)    + (ghjgaga**2.0 - 1)*SMBR_Hgamgam(m)+&
!           & (ghjZga**2.0    - 1)*SMBR_HZgam(m)  +&
!           & (ghjbb**2.0     - 1)*SMBR_Hbb(m)    +&
!           & (ghjtautau**2.0 - 1)*SMBR_Htautau(m)+&
!           & (ghjss**2.0     - 1)*SMBR_Hss(m)    +&
!           & (ghjmumu**2.0   - 1)*SMBR_Hmumu(m)  +&
!           & (ghjcc**2.0     - 1)*SMBR_Hcc(m)    +&
!           & (ghjtt**2.0     - 1)*SMBR_Htoptop(m) )
!  endif
! end subroutine get_totalWidth_effc8_2
!--------------------------------------------------------------------------------
function invbsq(mf,mh)
  implicit none
  double precision,intent(in) :: mf,mh
  double precision :: invbsq
  if(mh>2.0D0*mf)then
     invbsq=1.0D0/(1.0D0-4.0D0*(mf/mh)**2.0D0)
  else
     invbsq=0.0D0
  endif
end function invbsq
!--------------------------------------------------------------------------------
! subroutine input_decaymodes(m, scale_ZZ, scale_WW, scale_gaga, scale_tautau, scale_bb)
!   implicit none
!   double precision, intent(in) :: m, scale_ZZ, scale_WW, scale_gaga, scale_tautau, scale_bb
!   double precision :: CPValue
!   double precision :: SMBR_Htoptop,SMBR_Hss, SMBR_Hcc, SMBR_Hbb, SMBR_Hmumu, &
!        &     SMBR_Htautau, SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg
!   double precision :: SMGammaTotal, SMGamma_h
!   ! Entries of CS arrays: TEV, LHC7, LHC8, LHC13
!   double precision :: Mh,GammaTotal,CS_hj_ratio(3),   &
!        &          CS_gg_hj_ratio(3),CS_bb_hj_ratio(3),&
!        &          CS_hjW_ratio(3),CS_hjZ_ratio(3),    &
!        &          CS_vbf_ratio(3),CS_tthj_ratio(3),   &
!        &          CS_hjhi(3),CS_thj_schan_ratio(3),   &
!        &          CS_thj_tchan_ratio(3),              &
!        &          CS_qq_hjZ_ratio(3),CS_gg_hjZ_ratio(3),&
!        &          BR_hjss,BR_hjcc,BR_hjbb,&
!        &          BR_hjtt,BR_hjmumu,BR_hjtautau,&
!        &          BR_hjWW,BR_hjZZ,BR_hjZga,BR_hjgaga,BR_hjgg
!        !&          BR_hjinvisible,BR_hjhihi_nHbynH
!   integer :: collider, collider_s
!
!  Mh = m
!  SMGammaTotal=SMGamma_h(Mh)
!
!  if(.not. (SMGammaTotal .lt. 0)) then
!     GammaTotal=SMGammaTotal
!     ! CP even
!     CPValue=0.0d0
!     ! This applies to all 4 elements:
!     CS_hj_ratio=1.0D0
!     CS_gg_hj_ratio=1.0D0
!     CS_bb_hj_ratio=1.0D0
!     CS_hjW_ratio=1.0D0
!     CS_hjZ_ratio=1.0D0
!     CS_vbf_ratio=1.0D0
!     CS_tthj_ratio=1.0D0
!     CS_hjhi=0.0D0
!     CS_thj_tchan_ratio=1.0D0
!     CS_thj_schan_ratio=1.0D0
!     CS_qq_hjZ_ratio = 1.0D0
!     CS_gg_hjZ_ratio = 1.0D0
!
!     BR_hjss=SMBR_Hss(Mh)
!     BR_hjcc=SMBR_Hcc(Mh)
!     BR_hjbb=SMBR_Hbb(Mh)*scale_bb
!     BR_hjtt=SMBR_Htoptop(Mh)
!     BR_hjmumu=SMBR_Hmumu(Mh)
!     BR_hjtautau=SMBR_Htautau(Mh)*scale_tautau
!     BR_hjWW=SMBR_HWW(Mh)*scale_WW
!     BR_hjZZ=SMBR_HZZ(Mh)*scale_ZZ
!     BR_hjZga=SMBR_HZgam(Mh)
!     BR_hjgaga=SMBR_Hgamgam(Mh)*scale_gaga
!     BR_hjgg=SMBR_Hgg(Mh)
!
!     call HiggsBounds_neutral_input_properties(Mh,GammaTotal,CPValue)
!
!     do collider=1,3
!        select case(collider)
!        !case(1)
!        !   collider_s = 2
!        case(1)
!           collider_s = 7
!        case(2)
!           collider_s = 8
!        case(3)
!           collider_s = 13
!        end select
!
!        call HiggsBounds_neutral_input_hadr(collider_s,&
!             &          CS_hj_ratio(collider),  &
!             &          CS_gg_hj_ratio(collider),CS_bb_hj_ratio(collider),          &
!             &          CS_hjW_ratio(collider),CS_hjZ_ratio(collider),              &
!             &          CS_vbf_ratio(collider),CS_tthj_ratio(collider),             &
!             &          CS_thj_tchan_ratio(collider),CS_thj_schan_ratio(collider),  &
!             &          CS_qq_hjZ_ratio(collider),CS_gg_hjZ_ratio(collider),  &
!             &          CS_hjhi(collider))
!
!     end do
!
!     call HiggsBounds_neutral_input_SMBR(BR_hjss,BR_hjcc,BR_hjbb,BR_hjtt,   &
!          &                           BR_hjmumu,BR_hjtautau,BR_hjWW,BR_hjZZ,     &
!          &                           BR_hjZga,BR_hjgaga,BR_hjgg)
!
!
!  endif
!
! end subroutine input_decaymodes
!--------------------------------------------------------------------------------
! subroutine input_prodmodes_branchingratios(m,ggH,VBF,WH,ZH,ttH,br_ZZ,br_WW,br_gaga,br_tautau,br_bb,br_cc,br_gg,br_Zga)
!   implicit none
!   double precision, intent(in) :: m, ggH,VBF,WH,ZH,ttH ! XS ratios
!   double precision, intent(in) :: br_ZZ,br_WW,br_gaga,br_tautau,br_bb,br_cc,br_gg,br_Zga ! BRs
!   !double precision :: SMBR_Htoptop,SMBR_Hss, SMBR_Hcc, SMBR_Hbb, SMBR_Hmumu, &
!   !     &     SMBR_Htautau, SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg
!   double precision :: CPValue
!   double precision :: SMGammaTotal, SMGamma_h, s_bbH_SM
!   ! Entries of CS arrays: TEV, LHC7, LHC8, LHC13
!   double precision :: Mh,GammaTotal,CS_hj_ratio(4),   &
!        &          CS_gg_hj_ratio(4),CS_bb_hj_ratio(4),&
!        &          CS_hjW_ratio(4),CS_hjZ_ratio(4),    &
!        &          CS_vbf_ratio(4),CS_tthj_ratio(4),   &
!        &          CS_hjhi(4),CS_thj_schan_ratio(4),   &
!        &          CS_thj_tchan_ratio(4),              &
!        &          CS_qq_hjZ_ratio(4),CS_gg_hjZ_ratio(4),&
!        &          BR_hjss,BR_hjcc,BR_hjbb,&
!        &          BR_hjtt,BR_hjmumu,BR_hjtautau,&
!        &          BR_hjWW,BR_hjZZ,BR_hjZga,BR_hjgaga,BR_hjgg, &
!        &          BR_hjinvisible,BR_hjhihi_nHbynH
!   integer :: collider, collider_s
!
!  Mh = m
!  SMGammaTotal=SMGamma_h(Mh)
!
!  if(.not. (SMGammaTotal .lt. 0)) then
!     GammaTotal=SMGammaTotal
!     ! CP even
!     CPValue=0.0d0
!     ! This applies to all 4 elements:
!     CS_hj_ratio=1.0D0*ggH
!     CS_gg_hj_ratio=1.0D0*ggH
!     CS_bb_hj_ratio=1.0D0*ggH!1.0
!     CS_hjW_ratio=1.0D0*WH
!     CS_hjZ_ratio=1.0D0*ZH
!     CS_vbf_ratio=1.0D0*VBF
!     CS_tthj_ratio=1.0D0*ttH
!     CS_hjhi=0.0D0
!     CS_thj_tchan_ratio=1.0D0*ttH!1.0D0*ggH
!     CS_thj_schan_ratio=1.0D0*ttH!1.0D0*ggH
!     CS_qq_hjZ_ratio = 1.0D0
!     CS_gg_hjZ_ratio = 1.0D0
!
!
!     BR_hjtautau=br_tautau
!     BR_hjbb    =br_bb
!     BR_hjWW    =br_WW
!     BR_hjZZ    =br_ZZ
!     BR_hjgaga  =br_gaga
!     BR_hjcc    =br_cc
!     BR_hjgg    =br_gg
!     BR_hjZga   =br_Zga
!     BR_hjss    =0.0
!     BR_hjtt    =0.0
!     BR_hjmumu  =0.0
!
!
!     !BR_hjinvisible=0.0D0
!     !BR_hjhihi_nHbynH=0.0D0
!
!     call HiggsBounds_neutral_input_properties(Mh,GammaTotal,CPValue)
!
!     do collider=1,4
!        select case(collider)
!        case(1)
!           collider_s = 2
!        case(2)
!           collider_s = 7
!        case(3)
!           collider_s = 8
!        case(4)
!           collider_s = 13
!        end select
!
!        call HiggsBounds_neutral_input_hadr(collider_s,&
!             &          CS_hj_ratio(collider),  &
!             &          CS_gg_hj_ratio(collider),CS_bb_hj_ratio(collider),          &
!             &          CS_hjW_ratio(collider),CS_hjZ_ratio(collider),              &
!             &          CS_vbf_ratio(collider),CS_tthj_ratio(collider),             &
!             &          CS_thj_tchan_ratio(collider),CS_thj_schan_ratio(collider),  &
!             &          CS_qq_hjZ_ratio(collider),CS_gg_hjZ_ratio(collider),  &
!             &          CS_hjhi(collider))
!
!     end do
!
!     call HiggsBounds_neutral_input_SMBR(BR_hjss,BR_hjcc,BR_hjbb,BR_hjtt,   &
!          &                           BR_hjmumu,BR_hjtautau,BR_hjWW,BR_hjZZ,     &
!          &                           BR_hjZga,BR_hjgaga,BR_hjgg)
!  endif
!
! end subroutine input_prodmodes_branchingratios
!-------------------------------------------------------------------------------
subroutine input_effc(m,kt,ktodd,kV,kg,kga,BRNP)
  implicit none

  double precision, intent(in) :: m
  double precision, intent(in) :: kt,ktodd,kV,kg,kga,BRNP

  double precision :: ks, ksodd, kc, kcodd, kb, kbodd, kmu, kmuodd, ktau, ktauodd
  double precision :: kW, kZ, kZga

! If total width < 0, it will be derived from the provided effC and BR input
  call HiggsBounds_neutral_input_properties(m, -1.0D0 , 0.0)

  kW     = kV
  kZ     = kV
  kZga   = kV
  ks     = 1.0D0
  ksodd  = 0.0D0
  kc     = 1.0D0
  kcodd  = 0.0D0
  kb     = 1.0D0
  kbodd  = 0.0D0
  kmu     = 1.0D0
  kmuodd  = 0.0D0
  ktau    = 1.0D0
  ktauodd = 0.0D0


  call HiggsBounds_neutral_input_effC( &
       & ks,ksodd,kc,kcodd,&
       & kb,kbodd,kt,ktodd,&
       & kmu,kmuodd,ktau,ktauodd,&
       & kW,kZ,kZga ,&
       & kga,kg,0.0D0)


  !ghjggZ, BR_hjinvisible,BR_hjhihi)
! TS: This might work, but since we have no charged Higgs bosons, we maybe have to remove
!     BR_hjHpiW from the list of arguments -->
! Use invisible decay mode to provide the NP decay mode. This
! may still be not invisible if explicit constraints on BRinv are ignored.
  call HiggsBounds_neutral_input_nonSMBR(BRNP, 0.0, 0.0, 0.0, 0.0, 0.0)

end subroutine input_effC

subroutine input_ppZH(kappaV, kappaggZH)
 use theory_XS_SM_functions
 implicit none
 double precision, intent(in) :: kappaV, kappaggZH
 double precision :: mH, qqZH_SM, bbZH_SM, ggZH_SM, ppZH_model, mu_ppZH
 character(LEN=5) :: collider
 ! double precision :: ZH_cpmix_nnlo_qq, ZH_cpmix_nnlo_bb, ZH_cpmix_nnlo_gg

 collider = "LHC13"

  mH = 125.

  qqZH_SM = ZH_cpmix_nnlo_qq(mH, collider, 1.0D0, 1.0D0, 1.0D0, 0.0D0, 0.0D0)
  bbZH_SM = ZH_cpmix_nnlo_bb(mH, collider, 1.0D0, 0.0D0)
  ggZH_SM = ZH_cpmix_nnlo_gg(mH, collider, 1.0D0, 1.0D0, 1.0D0, 0.0D0, 0.0D0)

  write(*,*) qqZH_SM, ggZH_SM

  ppZH_model = ZH_cpmix_nnlo_qq(mH, collider, kappaV, 1.0D0, 1.0D0, 0.0D0, 0.0D0) + &
               ZH_cpmix_nnlo_bb(mH, collider, 1.0D0, 0.0D0) + &
               kappaggZH**2.*ggZH_SM

  mu_ppZH = ppZH_model/(qqZH_SM + bbZH_SM + ggZH_SM)

    call HiggsBounds_neutral_input_hadr_single(7, "XS_hjZ_ratio", mu_ppZH)
    call HiggsBounds_neutral_input_hadr_single(8, "XS_hjZ_ratio", mu_ppZH)
    call HiggsBounds_neutral_input_hadr_single(13, "XS_hjZ_ratio", mu_ppZH)

end subroutine input_ppZH

subroutine input_ggZH(kappaggZH)
  implicit None

  double precision, intent(in) :: kappaggZH

  call HiggsBounds_neutral_input_hadr_single(7, "XS_gg_hjZ_ratio", kappaggZH**2.0D0)
  call HiggsBounds_neutral_input_hadr_single(8, "XS_gg_hjZ_ratio", kappaggZH**2.0D0)
  call HiggsBounds_neutral_input_hadr_single(13, "XS_gg_hjZ_ratio", kappaggZH**2.0D0)

end subroutine input_ggZH

subroutine process_input
 use theo_manip, only : HB5_complete_theo
 implicit none

  call HB5_complete_theo

end subroutine process_input

subroutine get_totalwidth(GammaTot, GammaTot_SM)
  implicit none

  double precision, intent(out) :: GammaTot, GammaTot_SM

  call HiggsBounds_get_neutral_total_width(1, GammaTot, GammaTot_SM)

end subroutine get_totalwidth

subroutine assign_modfactor(obsID,modification_factor,Nc)
  use STXS, only: assign_modification_factor_to_STXS

  implicit none
  integer, intent(in) :: obsID
  integer, intent(in) :: Nc
  double precision, dimension(1, Nc), intent(in) :: modification_factor

  ! write(*,*) "obsID, Nc, modification factor = ", obsID, Nc, modification_factor
  call assign_modification_factor_to_STXS(obsID, Nc, modification_factor)

end subroutine assign_modfactor
! subroutine input_effC8_totalwidth(m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghzga,ghgg,alpha,cG,cB,cW,GammaTotal)
!   implicit none
!
!   double precision, intent(in) :: m
!   double precision, dimension(2), intent(in) :: ghuu,ghdd,ghll
!   double precision, intent(in) :: ghWW,ghZZ,ghgaga,ghzga,ghgg,alpha,cG,cB,cW,GammaTotal
!
!   double precision :: ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,ghjbb_s,ghjbb_p,&
!                     & ghjtt_s,ghjtt_p,ghjmumu_s,ghjmumu_p,ghjtautau_s,ghjtautau_p,&
!                     & ghjWW,ghjZZ,ghjZga,&
!                     & ghjgaga,ghjgg,&!,ghjggZ
!                     & ghjhiZ,BR_hjhihi,BR_hjinvisible, BR_hkhjhi, BR_hjhiZ,&
!                     & BR_hjemu, BR_hjetau, BR_hjmutau, BR_hjHpiW,&
!                     & Rcc,Rbb,Rgg,cWsq,sWsq
!
!   double precision :: invbsq
!
!   !-HiggsBounds internal functions to obtain SM branching ratios
!   double precision :: GammaTotal_fromCoup, CPValue
!   double precision :: SMBR_Htoptop,SMBR_Hss,SMBR_Hcc,SMBR_Hbb,SMBR_Hmumu,SMBR_Htautau,&
!                     & SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg,SMGamma_h
!   ! Definition of Fermion masses
!   double precision :: ms,mc,mb,mt,mmu,mtau,pi
!   ms=0.105d0
!   mc=1.27d0
!   mb=4.20d0
!   mt=173.34d0
!   mmu=105.7d-3
!   mtau=1.777d0
!
!   pi = 3.1415926535897931
!
!   !Weak mxing angle squared
!   cWsq = 0.769
!   sWsq = 0.231
!
!   !preliminary defintion of Rcc,Rbb and Rgg
!   Rcc = 1.0d0
!   Rbb = 1.0d0
!   Rgg = 1.0d0
!
!   if(SMGamma_h(m).ge.0.0D0) then
!      ghjss_s = cos(alpha)*ghdd(1)
!      ghjss_p = sin(alpha)*ghdd(2)
!      ghjcc_s =     cos(alpha)*ghuu(1)
!      ghjcc_p = Rcc*sin(alpha)*ghuu(2) !Rcc not correctly defined yet (Rcc=1 so far)
!      ghjbb_s =     cos(alpha)*ghdd(1)
!      ghjbb_p = Rbb*sin(alpha)*ghdd(2) !Rbb not correctly defined yet (Rbb=1 so far)
!      ghjtt_s =     cos(alpha)*ghuu(1)
!      ghjtt_p =     sin(alpha)*ghuu(2)
!      ghjmumu_s   = cos(alpha)*ghll(1)
!      ghjmumu_p   = sin(alpha)*ghll(2)
!      ghjtautau_s = cos(alpha)*ghll(1)
!      ghjtautau_p = sin(alpha)*ghll(2)
!
!      ghjWW = sqrt( (cos(alpha)*ghWW)**2 + (sin(alpha)*cW/(4*pi)**2)**2 * 0.155 )
!      ghjZZ = sqrt( (cos(alpha)*ghZZ)**2 + (sin(alpha)*(cB*sWsq+cW*cWsq)/(4*pi)**2)**2 * 0.074 )
!      ghjZga = cos(alpha)*ghzga ! no sensitive observable yet.
!      !Rgg not correctly defined yet (Rgg=1 so far)!
!      ghjgg   = ghgg
! !     ghjggZ  = cos(alpha)*ghZZ
!      ghjhiZ  = 0.0d0
!      ghjgaga = ghgaga
!
!      BR_hjhihi=0.0d0
!      BR_hkhjhi=0.0d0
!      BR_hjhiZ=0.0d0
!      BR_hjemu=0.0d0
!      BR_hjetau=0.0d0
!      BR_hjmutau=0.0d0
!      BR_hjHpiW=0.0d0
!
!      CPValue=0.0d0
!
!      !----Calculate the new total decay width:
!      GammaTotal_fromCoup = SMGamma_h(m)*(1 +&
!           & (ghjWW**2.0                                          - 1)*SMBR_HWW(m)    + (ghjZZ**2.0   - 1)*SMBR_HZZ(m) +&
!           & (ghjgg**2.0                                          - 1)*SMBR_Hgg(m)    + (ghjgaga**2.0 - 1)*SMBR_Hgamgam(m)+&
!           & (ghjZga**2.0                                         - 1)*SMBR_HZgam(m)  +&
!           & (ghjbb_s**2.0     + ghjbb_p**2.0*invbsq(mb,m)        - 1)*SMBR_Hbb(m)    +&
!           & (ghjtautau_s**2.0 + ghjtautau_p**2.0*invbsq(mtau,m)  - 1)*SMBR_Htautau(m)+&
!           & (ghjss_s**2.0     + ghjss_p**2.0*invbsq(ms,m)        - 1)*SMBR_Hss(m)    +&
!           & (ghjmumu_s**2.0   + ghjmumu_p**2.0*invbsq(mmu,m)     - 1)*SMBR_Hmumu(m)  +&
!           & (ghjcc_s**2.0     + ghjcc_p**2.0*invbsq(mc,m)        - 1)*SMBR_Hcc(m)    +&
!           & (ghjtt_s**2.0     + ghjtt_p**2.0*invbsq(mt,m)        - 1)*SMBR_Htoptop(m) )
!
!      if(GammaTotal.ge.GammaTotal_fromCoup) then
!         BR_hjinvisible=1.0d0 - GammaTotal_fromCoup/GammaTotal
!      else
!         write(*,*) "WARNING: GammaTotal is less than what is derived from the couplings!"
!         write(*,*) GammaTotal-GammaTotal_fromCoup
!         BR_hjinvisible=0.0d0
!      endif
!
!
!      call HiggsBounds_neutral_input_properties(m,GammaTotal,CPValue)
!      call HiggsBounds_neutral_input_effC(&
!           & ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,&
!           & ghjbb_s,ghjbb_p,ghjtt_s,ghjtt_p,&
!           & ghjmumu_s,ghjmumu_p,ghjtautau_s,ghjtautau_p,&
!           & ghjWW,ghjZZ,ghjZga,&
!           & ghjgaga,ghjgg,ghjhiZ)
!      !ghjggZ, BR_hjinvisible,BR_hjhihi)
! ! TS: This might work, but since we have no charged Higgs bosons, we maybe have to remove
! !     BR_hjHpiW from the list of arguments -->
!      call HiggsBounds_neutral_input_nonSMBR(BR_hjinvisible,BR_hkhjhi,BR_hjhiZ,&
!           &                                    BR_hjemu,BR_hjetau,BR_hjmutau,BR_hjHpiW)
!
!   endif
! end subroutine input_effC8_totalwidth
!-------------------------------------------------------------------------------
! subroutine input_effC8_totalwidth_2(m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghZga,ghgg,alpha,GammaTotal)
!   implicit none
!
!   double precision, intent(in) :: m,ghuu,ghdd,ghll,ghWW,ghZZ,ghgaga,ghZga,ghgg,alpha,GammaTotal
!
!   double precision :: ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,ghjbb_s,ghjbb_p,&
!                     & ghjtt_s,ghjtt_p,ghjmumu_s,ghjmumu_p,ghjtautau_s,ghjtautau_p,&
!                     & ghjWW,ghjZZ,ghjZga,&
!                     & ghjgaga,ghjgg,&
!                     & ghjhiZ,BR_hjhihi,BR_hjinvisible, BR_hkhjhi, BR_hjhiZ,&
!                     & BR_hjemu, BR_hjetau, BR_hjmutau, BR_hjHpiW
!   !ghjggZ
!   double precision :: invbsq
!
!   !-HiggsBounds internal functions to obtain SM branching ratios
!   double precision :: GammaTotal_fromCoup,CPValue
!   double precision :: SMBR_Htoptop,SMBR_Hss,SMBR_Hcc,SMBR_Hbb,SMBR_Hmumu,SMBR_Htautau,&
!                     & SMBR_HWW, SMBR_HZZ, SMBR_HZgam, SMBR_Hgamgam, SMBR_Hgg,SMGamma_h
!   ! Definition of Fermion masses
!   double precision :: ms,mc,mb,mt,mmu,mtau
!   ms=0.105d0
!   mc=1.27d0
!   mb=4.20d0
!   mt=173.34d0
!   mmu=105.7d-3
!   mtau=1.777d0
!
!   if(SMGamma_h(m).ge.0.0D0) then
!      ghjss_s = ghdd
!      ghjss_p = 0.0d0
!      ghjcc_s = ghuu
!      ghjcc_p = 0.0d0
!      ghjbb_s = ghdd
!      ghjbb_p = 0.0d0
!      ghjtt_s = ghuu
!      ghjtt_p = 0.0d0
!      ghjmumu_s = ghll
!      ghjmumu_p = 0.0d0
!      ghjtautau_s= ghll
!      ghjtautau_p= 0.0d0
!
!      ghjWW = cos(alpha)*ghWW
!      ghjZZ = cos(alpha)*ghZZ
!      ghjZga = 1.0d0
!      ghjgg = ghgg
! !     ghjggZ = cos(alpha)*ghZZ
!      ghjhiZ = 0.0d0
!      ghjgaga = ghgaga
!      BR_hjhihi=0.0d0
!      BR_hkhjhi=0.0d0
!      BR_hjhiZ=0.0d0
!      BR_hjemu=0.0d0
!      BR_hjetau=0.0d0
!      BR_hjmutau=0.0d0
!      BR_hjHpiW=0.0d0
!
!      CPValue=0.0d0
!
!      !----Calculate the new total decay width:
!      GammaTotal_fromCoup = SMGamma_h(m)*(1 +&
!           & (ghjWW**2.0                                         - 1)*SMBR_HWW(m)    + (ghjZZ**2.0   - 1)*SMBR_HZZ(m) +&
!           & (ghjgg**2.0                                         - 1)*SMBR_Hgg(m)    + (ghjgaga**2.0 - 1)*SMBR_Hgamgam(m)+&
!           & (ghjZga**2.0                                        - 1)*SMBR_HZgam(m)  +&
!           & (ghjbb_s**2.0     + ghjbb_p**2.0*invbsq(mb,m)       - 1)*SMBR_Hbb(m)    +&
!           & (ghjtautau_s**2.0 + ghjtautau_p**2.0*invbsq(mtau,m) - 1)*SMBR_Htautau(m)+&
!           & (ghjss_s**2.0     + ghjss_p**2.0*invbsq(ms,m)       - 1)*SMBR_Hss(m)    +&
!           & (ghjmumu_s**2.0   + ghjmumu_p**2.0*invbsq(mmu,m)    - 1)*SMBR_Hmumu(m)  +&
!           & (ghjcc_s**2.0     + ghjcc_p**2.0*invbsq(mc,m)       - 1)*SMBR_Hcc(m)    +&
!           & (ghjtt_s**2.0     + ghjtt_p**2.0*invbsq(mt,m)       - 1)*SMBR_Htoptop(m) )
!
!      if(GammaTotal.ge.GammaTotal_fromCoup) then
!         BR_hjinvisible=1.0d0 - GammaTotal_fromCoup/GammaTotal
!      else
!         write(*,*) "WARNING: GammaTotal is less than what is derived from the couplings!"
!         write(*,*) GammaTotal-GammaTotal_fromCoup
!         BR_hjinvisible=0.0d0
!      endif
!
!      call HiggsBounds_neutral_input_properties(m,GammaTotal,CPValue)
!      call HiggsBounds_neutral_input_effC(&
!           & ghjss_s,ghjss_p,ghjcc_s,ghjcc_p,&
!           & ghjbb_s,ghjbb_p,ghjtt_s,ghjtt_p,&
!           & ghjmumu_s,ghjmumu_p,ghjtautau_s,ghjtautau_p,&
!           & ghjWW,ghjZZ,ghjZga,&
!           & ghjgaga,ghjgg,ghjhiZ)
!      !ghjggZ, BR_hjinvisible,BR_hjhihi)
! ! TS: This might work, but since we have no charged Higgs bosons, we maybe have to remove
! !     BR_hjHpiW from the list of arguments -->
!      call HiggsBounds_neutral_input_nonSMBR(BR_hjinvisible,BR_hkhjhi,BR_hjhiZ,&
!           &                                    BR_hjemu,BR_hjetau,BR_hjmutau,BR_hjHpiW)
!
!   endif
! end subroutine input_effC8_totalwidth_2
!--------------------------------------------------------------------------------
subroutine run(Chisq_peak_mu, Chisq_peak_mh, Chisq_peak, nobs_peak, Pvalue_peak)
  implicit none
  double precision, intent(out) :: Pvalue_peak,Chisq_peak,Chisq_peak_mu,Chisq_peak_mh
  integer, intent(out) :: nobs_peak
  call run_HiggsSignals(Chisq_peak_mu, Chisq_peak_mh, Chisq_peak, nobs_peak, Pvalue_peak)
end subroutine run
!--------------------------------------------------------------------------------
subroutine run_lhcrun1(Chisq_mu, Chisq_mh, Chisq, nobs, Pvalue)
  implicit none
!   integer, intent(in) :: mode
  double precision, intent(out) :: Chisq_mu, Chisq_mh, Chisq, Pvalue
  integer, intent(out) :: nobs
  call run_HiggsSignals_LHC_Run1_combination(Chisq_mu, Chisq_mh, Chisq, nobs, Pvalue)
end subroutine run_lhcrun1
!--------------------------------------------------------------------------------
subroutine run_stxs(Chisq_STXS_rates, Chisq_STXS_mh, Chisq_STXS, nobs_STXS, Pvalue_STXS)
  implicit none
!   integer, intent(in) :: mode
  double precision, intent(out) :: Pvalue_STXS,Chisq_STXS,Chisq_STXS_rates,Chisq_STXS_mh
  integer, intent(out) :: nobs_STXS
  call run_HiggsSignals_STXS(Chisq_STXS_rates, Chisq_STXS_mh, Chisq_STXS, nobs_STXS, Pvalue_STXS)
end subroutine run_stxs
!--------------------------------------------------------------------------------
subroutine finish
  implicit none
  call finish_HiggsSignals
end subroutine finish
!--------------------------------------------------------------------------------
subroutine getID(ii, ID)
  use io, only : get_ID_of_peakobservable
  implicit none
  integer, intent(in) :: ii
  integer, intent(out) :: ID

  call get_ID_of_peakobservable(ii, ID)
end subroutine getID
!--------------------------------------------------------------------------------
function rate_fct(IDchannels,Nchannels)
  implicit none
  integer, intent(in) :: Nchannels
  character*5, dimension(Nchannels), intent(in) :: IDchannels
  double precision :: rate_fct
  ! write(*,*) "IDchannels, Nchannels = ",IDchannels, Nchannels
  call get_rates(1,4,Nchannels,IDchannels,rate_fct)
end function rate_fct
!--------------------------------------------------------------------------------
subroutine setToys(ID, mu_obs, mh_obs)
  implicit none
  integer, intent(in) :: ID
  double precision, intent(in) :: mh_obs, mu_obs
  call assign_toyvalues_to_peak(ID, mu_obs, mh_obs)
end subroutine setToys

!--------------------------------------------------------------------------------
! subroutine scale_error(ID, scale_mu)
!   implicit none
!   integer, intent(in) :: ID
!   double precision, intent(in) :: scale_mu
!   call assign_rate_uncertainty_scalefactor_to_peak(ID, scale_mu)
! end subroutine scale_error
!--------------------------------------------------------------------------------
subroutine getNobs(ntotal, npeakmu, npeakmh, nmpred, nanalyses)
  use io, only : get_number_of_observables
  implicit none
  integer, intent(out) :: ntotal, npeakmu, npeakmh, nmpred, nanalyses
  call get_number_of_observables(ntotal, npeakmu, npeakmh, nmpred, nanalyses)
end subroutine getNobs
!--------------------------------------------------------------------------------
subroutine getobs(obsID, muobs, dmuup, dmulow, mpeak, dm)
  use io, only : get_peakinfo
  implicit none
  integer, intent(in) :: obsID
  double precision, intent(out) :: muobs, dmuup, dmulow, mpeak, dm
  call get_peakinfo(obsID, muobs, dmuup, dmulow, mpeak, dm)
end subroutine getobs
!--------------------------------------------------------------------------------
subroutine getpred(obsID, mupred, domH, nHcomb)
  use io, only : get_peakinfo_from_HSresults
  implicit none
  integer, intent(in) :: obsID
  double precision, intent(out) :: mupred
  integer, intent(out) :: domH, nHcomb
  call get_peakinfo_from_HSresults(obsID, mupred, domH, nHcomb)
end subroutine getpred
!--------------------------------------------------------------------------------
subroutine get_covariancematrix(M,dim)
  use pc_chisq, only : get_cov_mu
  implicit none
  integer, intent(in) :: dim
  double precision, dimension(dim,dim), intent(out) :: M
  call get_cov_mu(M)
end subroutine get_covariancematrix
!--------------------------------------------------------------------------------
subroutine get_covariancematrix_mh(M,dim)
  use pc_chisq, only : get_cov_mh
  implicit none
  integer, intent(in) :: dim
  double precision, dimension(dim,dim), intent(out) :: M
  call get_cov_mh(1,M)
end subroutine get_covariancematrix_mh
!--------------------------------------------------------------------------------
subroutine print_covariancematrix
  use pc_chisq, only : print_cov_mu_to_file
  implicit none
  call print_cov_mu_to_file
end subroutine print_covariancematrix
!--------------------------------------------------------------------------------
subroutine set_assignmentrange(range)
  implicit none
  double precision, intent(in) :: range
  call setup_assignmentrange(range)
end subroutine set_assignmentrange
!--------------------------------------------------------------------------------
subroutine setup_output_lvl(level)
  implicit none
  integer, intent(in) :: level
  call setup_output_level(level)
end subroutine setup_output_lvl
