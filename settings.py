# Some global setting for the fit
datadir = "model_1/run_1/"

derive_loopcouplings = False
free_loopcouplings = True
free_kappagamma = False
account_for_kinematics = False
free_ggZH = False

# Number of sampled points
Ndraws = 100
# Number of burn-in points before sampling
Ntune = 500
# Number of parallel chains
NChains = 2
