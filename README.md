# Higgs-CP fitting framework #

This is the code to run the fits of the CP properties of the Higgs-top quark interaction, presented in arXiv:2007.08542 [hep-ph].

## Installation

1. In ```HiggsBounds``` directory, do:

```
   tar xvzf higgsbounds-version.tar.gz
   cd higgsbounds
   mkdir build
   cd build
   cmake .. -DCMAKE_Fortran_FLAGS="-fPIC"
   make
```
2. In ```HiggsSignals``` directory, do:
```
   tar xvzf higgssignals-version.tar.gz
   cd higgssignals
   mkdir build
   cd build
   cmake .. -DCMAKE_Fortran_FLAGS="-fPIC" -DHiggsBounds_DIR=/path/to/working/directory/HiggsBounds/higgsbounds/build/
   make
```

3. We can now create the python module for HiggsSignals:

   Edit the HiggsBounds and HiggsSignals paths in ```makefile.f2py```. Then
```
  make -f makefile.f2py
```  
   This will hopefully be successful and will create a .so file. In order to test it, we can go back into the parent (working) directory, and try in a python environment:
```
   import HiggsSignals.HS as HS
```


## Running the fit

The main program to perform the scan is

```
   scan.py
```
Before running it we need to adjust some global parameters in
```
   settings.py
```
that handle some model-specific features, e.g., whether loop-induced couplings should be derived from tree-level couplings, see
```
  model.py
```
for details.    

## Analyzing the data

The main analyzing script is

```
   analysis.py
```
which includes several model-specific settings at the beginning. Also, it can create 1D profiled data output for the CP-phase plot, which can be plotted with
```
   plot_1Dprofile.py
```

The script
```
   plot_projection.py
```
is essentially a modified copy of the analysis.py script, which overlays the figures with a projection band.
