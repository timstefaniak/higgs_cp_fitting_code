import numpy as np

def f(c1,c2,p1,p2,n):
    return c1/(n-p1) / (c2/(n - p2))

csq = [84.317, 84.300,  84.281,  84.075, 84.056, 61.55, 84.009, 61.488]
nobs = [106, 106,  106, 106, 106, 77, 106, 77]
npar = [2, 3, 4, 5, 5, 5, 6, 6]

csm = [84.45, 84.45, 84.45,84.45,84.45, 64.10, 84.45,64.10]

for i,c in enumerate(csq):
    print(f(csm[i],csq[i],0,npar[i],nobs[i]))
