import argparse

import matplotlib.pyplot as plt
import matplotlib as mpl

from glob import glob
from pandas.core.common import flatten

from random import seed, random, gauss
import numpy as np

import csv
import multiprocessing as mp

parser = argparse.ArgumentParser(description="Script to reprocess pyMC3-traces or generate new random samples.")
parser.add_argument('model', type=int, help='Model (1,2 or 4)')
parser.add_argument('part', type=int, help='Random sample part number')
parser.add_argument('npoints', type=int, help='Number of scan points')
args = parser.parse_args()

modelnr = args.model
part = args.part
Npoints = args.npoints

print("Starting with model ",modelnr,", part ",part," with ", Npoints, " scan points.")

# !!! GLOBAL BOOLEAN FLAGS HAVE TO BE SET BY HAND IN settings.py !!!

from model import *

output_file_append = "_random_"+str(part)+".tsv"
kV = 1.
kgeven = 0
kgodd = 0
kgaeven = 0
kgaodd = 0
BRNP = 0
model_dir = ""
output_file = ""

if modelnr == 0:
	model_dir = "model_0/"
	output_file = "data/model_0"+output_file_append
	par_names = ['kteven','ktodd']
	parameters = []

	pars_lower = [-2., -2.]
	pars_upper = [+2., +2.]
	par_type = [0,0]
		# 0: uniform with lower + upper boundaries
		# 1: Gaussian with mean (pars_lower) + sigma (pars_upper)


if modelnr == 1:
	model_dir = "model_1/"
	output_file = "data/model_1"+output_file_append
	par_names = ['kV','kteven','ktodd']
	parameters = []

	pars_lower = [0.5, -2., -2.]
	pars_upper = [1.5, +2., +2.]
	par_type = [0,0,0]
		# 0: uniform with lower + upper boundaries
		# 1: Gaussian with mean (pars_lower) + sigma (pars_upper)

if modelnr == 2:
	model_dir = "model_2/"
	output_file = "data/model_2"+output_file_append
	print(output_file)
	par_names = ['kV','kteven','ktodd','kga']
	parameters = []

	pars_lower = [0.7, -2., -2., 1.0]
	pars_upper = [1.3, +2., +2., 0.2]
	par_type = [0,0,0,1]
		# 0: uniform with lower + upper boundaries
		# 1: Gaussian with mean (pars_lower) + sigma (pars_upper)

if modelnr == 3:
	model_dir = "model_3/"
	output_file = "data/model_3"+output_file_append
	print(output_file)
	par_names = ['kV','kteven','ktodd','kg','kga']
	parameters = []

	#pars_lower = [0.9, 0.4, -1.5, 1.0, 1.0]
	#pars_upper = [1.1, 1.3, +1.5, 0.2, 0.2]
	pars_lower = [0.7, -2., -2., 1.0, 1,0]
	pars_upper = [1.3, +2., +2., 0.2, 0.2]
	par_type = [0,0,0,1,1]
		# 0: uniform with lower + upper boundaries
		# 1: Gaussian with mean (pars_lower) + sigma (pars_upper)

if modelnr == 4:
	model_dir = "model_4/"
	output_file = "data/model_4"+output_file_append
	print(output_file)
	par_names = ['kV','kteven','ktodd','kg','kga','kggZH']
	parameters = []

	pars_lower = [0.7, -3., -3.5, 1.0, 1.0, 0.]
	pars_upper = [1.3, +3., +3.5, 0.1, 0.1, 2.5]
	par_type = [0,0,0,1,1,0]
		# 0: uniform with lower + upper boundaries
		# 1: Gaussian with mean (pars_lower) + sigma (pars_upper)


def job(i):
	global pars_lower, pars_upper, par_type
	global kgeven, kgodd, kgaeven, kgaodd, BRNP, kV
	if(i%100 == 0):
		print(i)
	pars = []
	for j,p in enumerate(par_names):
		if par_type[j] == 0:
			pars.append(pars_lower[j] + random()*(pars_upper[j]-pars_lower[j]))
		elif par_type[j] == 1:
			pars.append(gauss(pars_lower[j],pars_upper[j]))
		else:
			print("Unknown parameter prior!")
	if modelnr == 0:
		theta = [kV,pars[0],pars[1],kgeven,kgodd,kgaeven,kgaodd,BRNP]
	elif modelnr == 1:
		theta = [pars[0],pars[1],pars[2],kgeven,kgodd,kgaeven,kgaodd,BRNP]
	elif modelnr == 2:
		theta = [pars[0],pars[1],pars[2],kgeven,kgodd,pars[3],kgaodd,BRNP]
	elif modelnr == 3:
		theta = [pars[0],pars[1],pars[2],pars[3],kgodd,pars[4],kgaodd,BRNP]
	elif modelnr == 4:
		theta = [pars[0],pars[1],pars[2],pars[3],pars[5],pars[4],kgaodd,BRNP]
	else:
		print("Wrong model.")

	HSresults = HSrun(theta)
	output = list(flatten([i,pars,HSresults]))

	return output


if __name__ == '__main__':

	pool = mp.Pool()


	with open(output_file,'w') as tsvfile:
		writer = csv.writer(tsvfile, delimiter='\t', lineterminator='\n')
		quantities_array = ["chi2tot","chi2peak","chi2run1","chi2stxs","kH",\
	                        "mu_all_gaga","mu_all_ZZ","mu_all_bb",\
	                        "mu_ppZH_bb","mu_qqZH_bb","mu_ggZH_bb",\
	                        "mu_ggH","mu_Hgaga","mu_ppZH","mu_qqZH","mu_ggZH",\
	                        "mu_tH","mu_ttH","mu_tWH","mu_ttHtWH","mu_tHttH",\
							"mu_topH","mu_tH_over_ttHtWH"]

		quantities_array.insert(0,par_names)
		quantities_array.insert(0,"N")

		output_line = list(flatten(quantities_array))
		writer.writerow(output_line)

		seed(None)
		output = pool.map(job, np.arange(Npoints))
		for element in output:
			writer.writerow(element)
