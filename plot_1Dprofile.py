import matplotlib.pyplot as plt
import matplotlib as mpl
import pylab as P
import pickle
import numpy as np
from scipy import stats
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

fs = 20
fs2 = 16
padsize = 6

shiftbins = True
fivemodels = False
#plt.style.use('default')
P.rc('text',usetex = True)
P.rc('text.latex',preamble = r"\usepackage{amsmath}\usepackage{color}")
# mpl.rcParams['text.latex.preamble'] = [ r'\usepackage{amsmath}',r'\usepackage{amssymb}']
plt.rcParams['figure.constrained_layout.use'] = True

font = {'size' : 18}
P.rc('font', **font)
P.rc('grid', linewidth=1,linestyle=":",color='#666666')

plt.rcParams['figure.constrained_layout.use'] = True
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
plt.rcParams['xtick.major.pad']=padsize
plt.rcParams['ytick.major.pad']=padsize

# output_dir = "results/model_"+str(model)+variant+"/"

if fivemodels:
    modelset = [0,1,2,3,4]
    bins = [50, 50, 25, 20, 20]
    labels = [r'$(c_t, \tilde{c}_{t})~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V)~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V, \kappa_\gamma)~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V, \kappa_\gamma, \kappa_g)~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V, \kappa_\gamma, \kappa_g, \kappa_{ggZH})~\text{free}$']
    legendbbtoa = (0.05,0.03,1.0,1)

else:
    modelset = [0,1,2,3]
    bins = [50, 50, 25, 20]
    labels = [r'$(c_t, \tilde{c}_{t})~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V)~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V, \kappa_\gamma)~\text{free}$',\
          r'$(c_t, \tilde{c}_{t}, c_V, \kappa_\gamma, \kappa_g)~\text{free}$']
    legendbbtoa = (0.055,0.025,1.0,1)
# lc = ['r','b','g','m']

phi = [[] for  m in modelset]
chi2tot = [[] for  m in modelset]

fig,ax = plt.subplots(figsize=(6,6))

for m in modelset:
    [phi[m], chi2tot[m]] = pickle.load(open( "data_1D/model_"+str(m)+"_phi_chi2tot.pkl", "rb" ) )

    shift = (max(phi[m]) - min(phi[m]))/(2*bins[m])
    binnedprofile, binedges, binnumber = stats.binned_statistic(phi[m],chi2tot[m],statistic='min',bins = bins[m])

    print("Model ", m)
    print(binedges)
    print(binnedprofile)
    chi2min = min(binnedprofile)
    Deltachi2 = list(map(lambda c: c-chi2min, binnedprofile))
    xvals = []
    if shiftbins:
        xvals = list(map(lambda x: x+shift, binedges[:-1]))
    else:
        xvals = binedges[:-1]
    ax.plot(xvals,Deltachi2,'-',label=labels[m])#,color=lc[m])
#
#
# ax.plot(par,Deltac2t,'.',color='lightgrey', zorder = 1, rasterized=True)
# ax.axvline(BF_vals[p],linewidth=2,color='grey' ,zorder = 5)
# print(p, min(par2s), max(par2s))
# ax.fill_betweenx([0,20.],[min(par2s),min(par2s)],[max(par2s),max(par2s)],color='yellow',alpha=0.3,zorder=3)
# ax.fill_betweenx([0,20.],[min(par1s),min(par1s)],[max(par1s),max(par1s)],color='green',alpha=0.3,zorder=3)
ax.hlines([1.0,4.0,9.0],-np.pi/2,np.pi/2,linestyles='dotted',colors='grey')
ax.set_xlim([-np.pi/2,np.pi/2])
ax.set_ylim([0.,15.])
ax.set_xlabel(r'$\alpha$')
ax.set_ylabel(r"$\Delta\chi^2$")
ax.tick_params(direction='in',which='major',length=6,width=1,grid_alpha=0.5)
ax.tick_params(direction='in',which='minor',length=3,width=1)
# if p in tick_dict.keys():
ax.xaxis.set_major_locator(MultipleLocator(np.pi/4.))

xlabels = [item.get_text() for item in ax.get_xticklabels()]
print(xlabels)
xlabels = ['',r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$',r'$0$', r'$\frac{\pi}{4}$', r'$\frac{\pi}{2}$','']
ax.set_xticklabels(xlabels)

ax.xaxis.set_minor_locator(AutoMinorLocator(5))
# if p[1] in tick_dict.keys():
ax.yaxis.set_major_locator(MultipleLocator(5))
ax.yaxis.set_minor_locator(AutoMinorLocator(5))
ax.text(0.95,0.085,r'$1\sigma$',fontsize=12,color='grey',transform=ax.transAxes)
ax.text(0.95,0.285,r'$2\sigma$',fontsize=12,color='grey',transform=ax.transAxes)
ax.text(0.95,0.61,r'$3\sigma$',fontsize=12,color='grey',transform=ax.transAxes)
# ax.text(0.02,0.94,addlabel,fontsize=fs2,transform=ax.transAxes)

legend = plt.legend(bbox_to_anchor=legendbbtoa,loc='upper center',bbox_transform=plt.gcf().transFigure,ncol=2,fontsize=11.6,borderpad=1.001)#,
legend.get_title().set_position((0,0))
legend.get_title().set_fontsize('14')
legend.set_zorder(15)

# plt.grid(zorder=20)
if fivemodels:
    fig.savefig("results/alpha_1D_5M.pdf",bbox_inches='tight')
else:
    fig.savefig("results/alpha_1D_4M.pdf",bbox_inches='tight')
