from glob import glob
import matplotlib.pyplot as plt
import matplotlib as mpl
import pylab as P
import pandas as pd
import numpy as np
from scipy import stats
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import multiprocessing as mp
import pickle

# from model import r_tH, r_ttH
# style = "scatter"

# Copied from model.py (no need to load the rest in that module!)
def sigma_tH(kteven,ktodd,kV):
    return 200.6*kteven**2.+233.8*kV**2.-373.2*kV*kteven+60.9*ktodd**2.

def sigma_ttH(kteven,ktodd):
    return 371.6*kteven**2.+155.8*ktodd**2.

def r_tH_func(kteven,ktodd,kV):
    return sigma_tH(kteven,ktodd,kV)/sigma_tH(1.,0.,1.)

def r_ttH(kteven,ktodd):
    return sigma_ttH(kteven,ktodd)/sigma_ttH(1.,0.)


style = "hexbins"
projection  = True
fs = 20
fs2 = 16
padsize =6
#
filter_in_range = True
filter_in_SMrates = False
draw_SM = True
draw_BF = True
add_contours = True
highlight_smlike_points = False
shiftbins = True # For SM-like band binning
shift_contour_bins = True
output_1d_profile = False
label = ""
model = 3
variant = ""
subdir = ""
#subdir = "model_1/"
#subdir = "model_1_negcV/"
preselection = False
chi2cut = 110
usepickle = False
savepickle = False
#modellabel = r"$(c_t, \tilde{c}_t)~\mathrm{free}$"
#modellabel = r"$(c_t, \tilde{c}_t, c_V)~\mathrm{free}$"
#modellabel = r"$(c_t, \tilde{c}_t, c_V, \kappa_\gamma)~\mathrm{free}$"
modellabel = r"$(c_t, \tilde{c}_t, c_V, \kappa_\gamma, \kappa_g)~\mathrm{free}$"
#modellabel = r"$(c_t, \tilde{c}_t, c_V, \kappa_\gamma, \kappa_g, \kappa_{ggZH})~\mathrm{free}$"
#addlabel = r"no $t\bar{t}H$ obs., no shape mod."
#addlabel="no shape mod."
addlabel=""
variant = "_fullset_projection_CPmixed"
subdir = "model_3_fullset/"

# variant = "_fullset_efftH_2"
# subdir = "model_3_fullset_efftH_2/"


#variant = "_fullset_nokin"
#subdir = "model_3_fullset_nokin/"
#subdir = "model_4_fullset_nokin/"

#variant = "_nottH_nokin"
#subdir = "model_3_nottH_nokin/"
#subdir = "model_4_nottH_nokin/"

# label = "_without_SMrates_filter"
# contourbins = [25,25]
# hexbins = [50,50]

contourbins = [50,50]
hexbins = [50,100]

ellipsecoeff = np.sqrt(1.5)
# SM predicted 13 TeV inclusive XS:
s_ttH = 0.4987
s_tH = 0.079

# projected measurement assuming  SM
#mu_tH_over_ttHtWH_proj_lower = 0.45
#mu_tH_over_ttHtWH_proj_upper = 1.89
# projected measurement assuming CP-mixed 2
mu_tH_over_ttHtWH_proj_lower = 2.44
mu_tH_over_ttHtWH_proj_upper = 4.80

#plt.style.use('default')
P.rc('text',usetex = True)
P.rc('text.latex',preamble = r"\usepackage{amsmath}\usepackage{color}")
font = {'size' : 18}
P.rc('font', **font)
P.rc('grid', linewidth=1,linestyle=":",color='#666666')

# mpl.rcParams['text.latex.preamble'] = [ r'\usepackage{amsmath}',r'\usepackage{amssymb}']
plt.rcParams['figure.constrained_layout.use'] = True
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
plt.rcParams['xtick.major.pad']=padsize
plt.rcParams['ytick.major.pad']=padsize

output_dir = "results/model_"+str(model)+variant+"/"

df = pd.DataFrame()

if usepickle:
    df = pd.read_pickle('data/'+subdir+"reduced_dataset_chi2_"+str(chi2cut)+'.pkl')
else:
    input_files = glob("data/"+subdir+"model_"+str(model)+"_random_*.tsv")
    files = [pd.DataFrame() for f in input_files]

    if preselection:
        df_array = [pd.DataFrame() for f in input_files]
        for i,f in enumerate(input_files):
            df_full = pd.read_csv(f,sep='\t',index_col=0)
            df_array[i] = df_full.loc[df_full.chi2tot <= chi2cut]
            print("Loaded ",f," with initial size ", df_full.size,". Reduced this with chi^2 selection to ", df_array[i].size)
            del df_full

        df = pd.concat(df_array).reset_index(drop=True)
    else:
        print("Loading ", input_files)
        files = [pd.read_csv(f,sep='\t',index_col=0) for f in input_files]
        df = pd.concat(files).reset_index(drop=True)

if savepickle:
    df.to_pickle('data/'+subdir+"reduced_dataset_chi2_"+str(chi2cut)+'.pkl')

if output_1d_profile:
    phi = list(map(lambda kt, kttilde: np.arcsin(kttilde/np.sqrt(kt**2.+kttilde**2)), df['kteven'],df['ktodd']))
    pickle.dump( [phi, df.chi2tot], open( "data_1D/model_"+str(model)+"_phi_chi2tot.pkl", "wb" ) )
    exit()

if model == 0:
    chi2profiles = ['kteven', 'ktodd','cosphi','phi']
    SM_vals = {'kteven' : 1, 'ktodd': 0, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_all_gaga' : 1, 'mu_Hgaga' : 1, 'mu_tH' : 1, 'mu_ttH' : 1, 'gamma' : 0, 'phi': 0, 'cosphi': 1.}
    plotpairings = [['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kteven','mu_qqZH'],['ktodd','mu_qqZH'],['kteven','mu_tH'],['ktodd','mu_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['gamma','mu_tH'],['gamma','mu_ttH'],['kteven','ktodd'],['mu_qqZH','mu_ggZH'],['mu_ggH','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kteven','mu_ggH'],['ktodd','mu_ggH'],['mu_ttH','mu_tH']]
    # plotpairings = [['kV','mu_all_gaga'],['kteven','mu_all_gaga'],['ktodd','mu_all_gaga'],]
    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','mu_qqZH':r'$R_{q\bar{q}\to ZH}$','mu_ggZH': r'$R_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$','mu_all_gaga': r'$R_{pp\to H\to\gamma\gamma}$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','mu_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H,tH}$','gamma' : r'$\gamma$', 'cosphi' : r'$\cos\phi$', 'phi' : r'$\phi$' }
    range_dict = {'kV': [0.7,1.2], 'kteven': [0.4,1.2], 'ktodd': [-1,1], 'mu_qqZH': [0.7,1.2], 'mu_ggZH': [0.4,1.8],'mu_ggH': [0.5,1.5], 'mu_all_gaga': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'mu_tH': [0.5,1.5], 'mu_ttH' : [0.5,1.5], 'mu_ttHtH' : [0.4,2.0],'gamma': [-np.pi/2.,np.pi/2.], 'cosphi': [0.5,1], 'phi' : [-np.pi/2.,np.pi/2.]}
    tick_dict = {'kV': [0.1,5],'kteven': [0.2,5], 'ktodd':  [0.5,5], 'cosphi': [0.1,5],'phi': [0.5,5]}

elif model == 1:
    chi2profiles = ['kV', 'kteven', 'ktodd']
    SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_all_gaga' : 1, 'mu_Hgaga' : 1, 'r_tH' : 1, 'mu_ttH' : 1, 'mu_ttHtH' : 1, 'gamma' : 0, 'r_tHttH' : 1, 'mu_tH_over_ttHtWH' : 1, 'mu_topH' : 1}
#    SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_all_gaga' : 1, 'mu_Hgaga' : 1, 'mu_tH_over_ttHtWH' : 1, 'mu_topH' : 1}
    if filter_in_SMrates:
        plotpairings = [['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH']]
    else:
        if highlight_smlike_points:
            plotpairings = [['ktodd','mu_tH_over_ttHtWH']]
#,['ktodd','mu_topH']]
        else:
            plotpairings = [['kteven','mu_tH_over_ttHtWH']]
#            plotpairings = [['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kV','mu_ggZH'],['kteven','mu_qqZH'],['ktodd','mu_qqZH'],['kV','mu_qqZH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['kV','mu_ttH'],['gamma','r_tH'],['gamma','mu_ttH'],['kV','kteven'],['kV','ktodd'],['kteven','ktodd'],['mu_qqZH','mu_ggZH'],['mu_ttH','r_tH'],['kV','r_tHttH'],['kteven','r_tHttH'],['ktodd','r_tHttH'],['mu_ttH','r_tHttH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','mu_tH_over_ttHtWH'],['kteven','mu_topH']]

    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','mu_qqZH':r'$\mu_{q\bar{q}\to ZH}$','mu_ggZH': r'$\mu_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$','mu_all_gaga': r'$R_{pp\to H\to\gamma\gamma}$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','r_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H+tH}$','gamma' : r'$\gamma$','r_tHttH' : r'$\mu_{tH/t\bar{t}H}$', 'mu_topH' : r'$\mu_{tH+t\bar{t}H+tWH}$','mu_tH_over_ttHtWH':r'$\mu_{tH/(t\bar{t}H+tWH)}$' }
    range_dict = {'kV': [0.7,1.2], 'kteven': [0.4,1.2], 'ktodd': [-1,1], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,1.8],'mu_ggH': [0.5,1.5], 'mu_all_gaga': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'r_tH': [0.5,2], 'mu_ttH' : [0.5,1.5], 'mu_ttHtH' : [0.4,1.6],'gamma': [-np.pi/2.,np.pi/2.], 'r_tHttH' : [0.,4.], 'mu_tH_over_ttHtWH' : [0.,4.], 'mu_topH' : [0.4,1.6]}
    tick_dict = {'kV': [0.1,5],'kteven': [0.2,5], 'ktodd':  [0.5,5], 'mu_qqZH' : [0.2,5],'mu_ggZH' : [0.2,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'r_tH' : [0.2,4],  'mu_ttH' : [0.2,5], 'mu_ttHtH' : [0.2,5] ,'r_tHttH' : [1,5], 'mu_tH_over_ttHtWH' : [1,5], 'mu_topH' : [0.2,5] }
#    range_dict = {'kV': [-1.2,-0.7], 'kteven': [-1.2,-0.4], 'ktodd': [-1,1], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,1.8],'mu_ggH': [0.5,1.5], 'mu_all_gaga': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'r_tH': [0.5,2], 'mu_ttH' : [0.5,1.5], 'mu_ttHtH' : [0.4,1.6],'gamma': [-np.pi/2.,np.pi/2.], 'r_tHttH' : [0.,4.], 'mu_tH_over_ttHtWH' : [0.,4.], 'mu_topH' : [0.4,1.6]}
#    tick_dict = {'kV': [0.1,5],'kteven': [0.2,5], 'ktodd':  [0.5,5], 'mu_qqZH' : [0.2,5],'mu_ggZH' : [0.2,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'r_tH' : [0.2,4],  'mu_ttH' : [0.2,5], 'mu_ttHtH' : [0.2,5] ,'r_tHttH' : [1,5], 'mu_tH_over_ttHtWH' : [1,5], 'mu_topH' : [0.2,5] }

elif model == 2:
    chi2profiles = ['kV', 'kteven', 'ktodd','kga']
    SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'kga': 1, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_all_gaga' : 1, 'mu_Hgaga' : 1, 'r_tH' : 1, 'mu_ttH' : 1, 'mu_ttHtH' : 1, 'r_tHttH' : 1, 'gamma' : 0}
    # SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'kga': 1, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_Hgaga' : 1, 'mu_tH' : 1, 'mu_ttH' : 1}
    if filter_in_SMrates:
        plotpairings = [['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH']]
    else:
        plotpairings = [['kV','kga'],['kteven','kga'],['ktodd','kga'],['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kV','mu_ggZH'],['kteven','mu_qqZH'],['ktodd','mu_qqZH'],['kV','mu_qqZH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['kV','mu_ttH'],['gamma','r_tH'],['gamma','mu_ttH'],['kV','kteven'],['kV','ktodd'],['kteven','ktodd'],['mu_qqZH','mu_ggZH'],['mu_ttH','r_tH'],['kga','r_tHttH'],['kV','r_tHttH'],['kteven','r_tHttH'],['ktodd','r_tHttH'],['r_tH','r_tHttH'],['mu_ttH','r_tHttH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH']]

    # plotpairings = [['kV','kteven'],['kV','ktodd'],['kteven','ktodd'],['kV','kga'],['kteven','kga'],['ktodd','kga'],['mu_qqZH','mu_ggZH'],['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH'],['mu_ttH','mu_tH']]
    # plotpairings = []
    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','kga':r'$\kappa_\gamma$','mu_qqZH':r'$R_{q\bar{q}\to ZH}$','mu_ggZH': r'$R_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$','mu_all_gaga': r'$R_{pp\to H\to\gamma\gamma}$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','r_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H+tH}$','gamma' : r'$\gamma$','r_tHttH' : r'$\mu_{tH/t\bar{t}H}$' }
    range_dict = {'kV': [0.7,1.2], 'kteven': [0.4,1.2], 'ktodd': [-1,1], 'kga': [0.8,1.2], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,1.8],'mu_ggH': [0.5,1.5], 'mu_all_gaga': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'r_tH': [0,4], 'mu_ttH' : [0.5,1.5], 'mu_ttHtH' : [0.4,2.0],'gamma': [-np.pi/2.,np.pi/2.], 'r_tHttH' : [0.,10.]}
    tick_dict = {'kV': [0.1,5],'kteven': [0.2,5], 'ktodd':  [0.5,5], 'kga':  [0.1,5], 'mu_qqZH' : [0.2,5],'mu_ggZH' : [0.2,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'r_tH' : [1,5],  'mu_ttH' : [0.2,5], 'mu_ttHtH' : [0.2,5], 'r_tHttH' : [2,5] }

    # label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','kga':r'$\kappa_\gamma$','mu_qqZH':r'$R_{q\bar{q}\to ZH}$','mu_ggZH': r'$R_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','mu_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$' }
    # range_dict = {'kV': [0.7,1.2], 'kteven': [0.4,1.2], 'ktodd': [-1,1],'kga': [0.8,1.2], 'mu_qqZH': [0.7,1.2], 'mu_ggZH': [0.6,1.4],'mu_ggH': [0.5,1.5], 'mu_all_gaga': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'mu_tH': [0.5,1.5], 'mu_ttH' : [0.5,1.5]}
    # tick_dict = {'kV': [0.1,5],'kteven': [0.2,5], 'ktodd':  [0.5,5], 'kga':  [0.1,5]}
    # range_dict = {'kV': [0.75,1.2], 'kteven': [-1.5,1.5], 'ktodd': [-2,2],'kga': [0.5,1.5], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,4.0],'mu_ggH': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'mu_tH': [0.5,5.], 'mu_ttH' : [0.5,1.5]}
    # tick_dict = {'kV': [0.1,5],'kteven': [0.5,5], 'ktodd':  [1.,5]}
elif model == 3:
    chi2profiles = ['kV', 'kteven', 'ktodd','kg','kga']
    SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'kg': 1, 'kga': 1, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_Hgaga' : 1, 'r_tH' : 1, 'mu_ttH' : 1, 'gamma' : 0, 'mu_ttHtH' : 1, 'r_tHttH' : 1, 'xsr_tHttH' : 1, 'mu_tH_over_ttHtWH' : 1, 'mu_topH' : 1}
    if filter_in_SMrates:
        plotpairings = [['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH']]
    else:
        # plotpairings = [['kteven','ktodd'],['kV','kga'],['kteven','kga'],['ktodd','kga'],['kV','kg'],['kteven','kg'],['ktodd','kg'],['kg','kga'],['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kV','mu_ggZH'],['kteven','mu_qqZH'],['ktodd','mu_qqZH'],['kV','mu_qqZH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['kV','mu_ttH'],['gamma','r_tH'],['gamma','mu_ttH'],['kV','kteven'],['kV','ktodd'],['mu_qqZH','mu_ggZH'],['mu_ttH','r_tH'], ['r_tH','r_tHttH'],['mu_ttH','r_tHttH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kg','r_tHttH'],['kga','r_tHttH'],['kV','r_tHttH'],['kteven','mu_tH_over_ttHtWH'],['kteven','mu_topH'],['ktodd','mu_tH_over_ttHtWH'],['ktodd','mu_topH']]
       plotpairings = [['kteven','ktodd'],['kteven','mu_tH_over_ttHtWH'],['ktodd','mu_tH_over_ttHtWH']]

        #plotpairings  = [['kteven','r_tHttH'],['ktodd','r_tHttH']]
        #plotpairings =  [['kg','r_tHttH'],['kga','r_tHttH'],['kV','r_tHttH'],['kteven','r_tHttH'],['ktodd','r_tHttH'],['kg','xsr_tHttH'],['kga','xsr_tHttH'],['kV','xsr_tHttH'],['kteven','xsr_tHttH'],['ktodd','xsr_tHttH']]
        #plotpairings = [['r_tH','r_tHttH'],['mu_ttH','r_tHttH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kg','r_tHttH'],['kga','r_tHttH'],['kV','r_tHttH'],['kteven','r_tHttH'],['ktodd','r_tHttH']]
    #plotpairings = [['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','mu_tH'],['ktodd','mu_tH'],['kV','mu_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['kV','mu_ttH'],['gamma','mu_tH'],['gamma','mu_ttH'],['kV','kteven'],['kV','ktodd'],['kteven','ktodd'],['kV','kg'],['kV','kga'],['kteven','kg'],['kteven','kga'],['ktodd','kg'],['ktodd','kga'],['mu_qqZH','mu_ggZH'],['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH'],['mu_ttH','mu_tH'],['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kV','mu_ggZH']]
    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','kg':r'$\kappa_g$','kga':r'$\kappa_\gamma$','mu_qqZH':r'$\mu_{q\bar{q}\to ZH}$','mu_ggZH': r'$\mu_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$','mu_all_gaga': r'$R_{pp\to H\to\gamma\gamma}$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','r_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H+tH}$','gamma' : r'$\gamma$', 'r_tHttH' : r'$\mu_{tH/t\bar{t}H}$' ,'xsr_tHttH' : r'$\mu_{tH/t\bar{t}H} \equiv \frac{\sigma_{tH}/\sigma_{t\bar{t}H}}{\sigma_{tH}^\text{SM}/\sigma_{t\bar{t}H}^\text{SM}}$', 'mu_topH' : r'$\mu_{tH+t\bar{t}H+tWH}$','mu_tH_over_ttHtWH':r'$\mu_{tH/(t\bar{t}H+tWH)}$'  }
#    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','kg':r'$\kappa_g$','kga':r'$\kappa_\gamma$','mu_qqZH':r'$R_{q\bar{q}\to ZH}$','mu_ggZH': r'$R_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','mu_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'gamma' : r'$\gamma$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H,tH}$' }
    range_dict = {'kV': [0.7,1.2], 'kteven': [-0.5,1.5], 'ktodd': [-2,2], 'kg': [0.,2.],'kga': [0,2.], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,5],'mu_ggH': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'r_tH': [0.,10.], 'mu_ttH' : [0.,5.0],'mu_ttHtH' : [0.,2.5], 'gamma': [-np.pi/2.,np.pi/2.], 'r_tHttH' : [0.,10], 'xsr_tHttH' : [0.,10.], 'mu_tH_over_ttHtWH' : [0.,10.], 'mu_topH' : [0.,2.5]}
    # range_dict = {'kV': [0.7,1.2], 'kteven': [-2.5,2.5], 'ktodd': [-3,3], 'kg': [0.5,1.5],'kga': [0.5,1.5], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.4,6.4],'mu_ggH': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'mu_tH': [0.,5.], 'mu_ttH' : [0.,5.0],'mu_ttHtH' : [0.,5.0], 'gamma': [-np.pi/2.,np.pi/2.]}
    tick_dict = {'kV': [0.1,5],'kteven': [0.5,5], 'ktodd':  [1,5], 'kg':  [0.2,5], 'kga':  [0.2,5], 'mu_qqZH' : [0.2,5],'mu_ggZH' : [1,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'r_tH' : [2,5],  'mu_ttH' : [1,5], 'mu_ttHtH' : [0.5,5], 'r_tHttH' : [2,5], 'xsr_tHttH' : [2.,5.], 'mu_tH_over_ttHtWH' : [2,5], 'mu_topH' : [0.5,5]}
    # tick_dict = {'kV': [0.1,5],'kteven': [1,5], 'ktodd':  [1,5], 'kg':  [0.2,5], 'kga':  [0.2,5], 'mu_qqZH' : [0.2,5],'mu_ggZH' : [1,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'mu_tH' : [1,5],  'mu_ttH' : [1,5], 'mu_ttHtH' : [1,5]}

elif model == 4:
    chi2profiles = ['kV', 'kteven', 'ktodd','kg','kga', 'kggZH']
    SM_vals = {'kV': 1, 'kteven' : 1, 'ktodd': 0, 'kg': 1, 'kga': 1, 'kggZH' : 1, 'mu_qqZH' : 1, 'mu_ggZH' : 1, 'mu_ggH' : 1, 'mu_Hgaga' : 1, 'r_tH' : 1, 'mu_ttH' : 1, 'gamma' : 0, 'mu_ttHtH' : 1, 'r_tHttH' : 1, 'xsr_tHttH' : 1}
    if filter_in_SMrates:
        plotpairings = [['mu_ggH','mu_Hgaga'],['kV','mu_Hgaga'],['kteven','mu_Hgaga'],['ktodd','mu_Hgaga'],['kV','mu_ggH'],['kteven','mu_ggH'],['ktodd','mu_ggH']]
    else:
        plotpairings = [['kteven','ktodd'],['mu_ttHtH','r_tHttH'],['kggZH','mu_ggZH'],['kg','kggZH'],['kga','kggZH'],['kV','kggZH'],['kteven','kggZH'],['ktodd','kggZH'],['kV','kga'],['kteven','kga'],['ktodd','kga'],['kV','kg'],['kteven','kg'],['ktodd','kg'],['kg','kga'],['kteven','mu_ggZH'],['ktodd','mu_ggZH'],['kV','mu_ggZH'],['kteven','mu_qqZH'],['ktodd','mu_qqZH'],['kV','mu_qqZH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kteven','mu_ttH'],['ktodd','mu_ttH'],['kV','mu_ttH'],['gamma','r_tH'],['gamma','mu_ttH'],['kV','kteven'],['kV','ktodd'],['mu_qqZH','mu_ggZH'],['mu_ttH','r_tH'], ['r_tH','r_tHttH'],['mu_ttH','r_tHttH'],['kteven','mu_ttHtH'],['ktodd','mu_ttHtH'],['kV','mu_ttHtH'],['kteven','r_tH'],['ktodd','r_tH'],['kV','r_tH'],['kg','r_tHttH'],['kga','r_tHttH'],['kV','r_tHttH'],['kteven','r_tHttH'],['ktodd','r_tHttH']]

    label_dict = {'kV':r'$c_V$','kteven':r'$c_t$','ktodd':r'$\tilde{c}_t$','kg':r'$\kappa_g$','kga':r'$\kappa_\gamma$','kggZH':r'$\kappa_{ggZH}$','mu_qqZH':r'$\mu_{q\bar{q}\to ZH}$','mu_ggZH': r'$\mu_{gg\to ZH}$', 'mu_ggH': r'$\kappa_g^2 \equiv \frac{\sigma}{\sigma_{\text{SM}}}(gg\to H)$','mu_all_gaga': r'$R_{pp\to H\to\gamma\gamma}$', 'mu_Hgaga': r'$r_\gamma \equiv \frac{\mathrm{BR}}{\mathrm{BR}_{\text{SM}}}(H\to \gamma\gamma)$','r_tH': r'$\mu_{tH}$','mu_ttH': r'$\mu_{t\bar{t}H}$', 'mu_ttHtH' : r'$\mu_{t\bar{t}H+tH}$','gamma' : r'$\gamma$', 'r_tHttH' : r'$\mu_{tH/t\bar{t}H}$' ,'xsr_tHttH' : r'$\mu_{tH+t\bar{t}H} \equiv \frac{\sigma_{tH}/\sigma_{t\bar{t}H}}{\sigma_{tH}^\text{SM}/\sigma_{t\bar{t}H}^\text{SM}}$' }
    range_dict = {'kV': [0.7,1.2], 'kteven': [-2,2], 'ktodd': [-2,2], 'kg': [0.,2.],'kga': [0,2.],'kggZH': [0,3.], 'mu_qqZH': [0.5,1.5], 'mu_ggZH': [0.0,8.],'mu_ggH': [0.5,1.5], 'mu_Hgaga': [0.5,1.5],'r_tH': [0.,10.], 'mu_ttH' : [0.,5.0],'mu_ttHtH' : [0.,2.5], 'gamma': [-np.pi/2.,np.pi/2.], 'r_tHttH' : [0.,10.], 'xsr_tHttH' : [0.,10.]}
    tick_dict = {'kV': [0.1,5],'kteven': [1,5], 'ktodd':  [1,5], 'kg':  [0.2,5], 'kga':  [0.2,5], 'kggZH' : [0.5,5],'mu_qqZH' : [0.2,5],'mu_ggZH' : [1,5], 'mu_ggH' : [0.2,5], 'mu_all_gaga' : [0.2,5], 'mu_Hgaga' : [0.2,5], 'r_tH' : [2,5],  'mu_ttH' : [1,5], 'mu_ttHtH' : [0.5,5], 'r_tHttH' : [2,5], 'xsr_tHttH' : [2.,5.]}

c2t = df['chi2tot']
c2t_min = c2t.min()
minindex = c2t.idxmin()
print("Minimal chi2:", c2t_min)

if filter_in_range:
    df = df.loc[(df.kteven >= range_dict['kteven'][0]) & (df.kteven <= range_dict['kteven'][1])]
    df = df.loc[(df.ktodd >= range_dict['ktodd'][0]) & (df.ktodd <= range_dict['ktodd'][1])]
    if model >= 1:
        df = df.loc[(df.kV >= range_dict['kV'][0]) & (df.kV <= range_dict['kV'][1])]
    if model == 2:
        df = df.loc[(df.kga >= range_dict['kga'][0]) & (df.kga <= range_dict['kga'][1])]
    if model >= 3:
        df = df.loc[(df.kg >= range_dict['kg'][0]) & (df.kg <= range_dict['kg'][1])]
        df = df.loc[(df.kga >= range_dict['kga'][0]) & (df.kga <= range_dict['kga'][1])]
if filter_in_SMrates:
    df = df.loc[(df.mu_ggH >= range_dict['mu_ggH'][0]) & (df.mu_ggH <= range_dict['mu_ggH'][1])]
    df = df.loc[(df.mu_Hgaga >= range_dict['mu_Hgaga'][0]) & (df.mu_Hgaga <= range_dict['mu_Hgaga'][1])]

#np.where(np.isclose(c2t,c2t_min,0.00000001))[0][0]

# PROBLEM: tH, ttH are not in fit sample.
parnames = list(SM_vals.keys())
if 'gamma' in SM_vals.keys():
    parnames.remove('gamma')
if 'mu_ttHtH' in SM_vals.keys():
    parnames.remove('mu_ttHtH')
if 'r_tHttH' in SM_vals.keys():
    parnames.remove('r_tHttH')
if 'r_tH' in SM_vals.keys():
    parnames.remove('r_tH')
if 'xsr_tHttH' in SM_vals.keys():
    parnames.remove('xsr_tHttH')
if 'phi' in SM_vals.keys():
    parnames.remove('phi')
if 'cosphi' in SM_vals.keys():
    parnames.remove('cosphi')

# parnames.remove('r_ttH')
if draw_BF:
    BF_vals = {k: df[k][minindex] for k in parnames}
    BF_vals.update({'gamma': np.arctan(ellipsecoeff*BF_vals['ktodd']/BF_vals['kteven'])})
    BF_vals.update({'cosphi': BF_vals['kteven']/np.sqrt(BF_vals['kteven']**2.+BF_vals['ktodd']**2)})
    BF_vals.update({'phi': np.arcsin(BF_vals['ktodd']/np.sqrt(BF_vals['kteven']**2.+BF_vals['ktodd']**2))})

    if model > 0:
        BF_vals.update({'r_tH': r_tH_func(BF_vals['kteven'], BF_vals['ktodd'], BF_vals['kV'])})
        BF_vals.update({'mu_ttHtH': (BF_vals['mu_ttH']*s_ttH+BF_vals['r_tH']*s_tH)/(s_ttH+s_tH)})
        BF_vals.update({'r_tHttH': BF_vals['r_tH']/BF_vals['mu_ttH']})
        BF_vals.update({'xsr_tHttH': ((BF_vals['r_tH']*s_tH)/(BF_vals['mu_ttH']*s_ttH))/(s_tH/s_ttH)})
    print(BF_vals)

# kV_BF = df['kV'][minindex]
# kteven_BF = df['kteven'][minindex]
# ktodd_BF = df['ktodd'][minindex]
# if model == 4:
#     kg_BF = df['kg'][minindex]
#     kga_BF = df['kga'][minindex]
#     print("Best fit point is number ",minindex," at kV, kt, kt~, kg, kga = ", kV_BF, kteven_BF, ktodd_BF,kg_BF, kga_BF," chi2 = ", c2t_min)
# else:
#     print("Best fit point is number ",minindex," at kV, kt, kt~ = ", kV_BF, kteven_BF, ktodd_BF," chi2 = ", c2t_min)
# r_ttH_BF = r_ttH(kteven_BF, ktodd_BF)
# r_tH_BF = r_tH(kteven_BF, ktodd_BF, kV_BF)

# print(kV_BF, minindex)

Deltac2t = list(map(lambda c: c-c2t_min, df.chi2tot))

df1s = df.loc[df.chi2tot <= c2t_min + 2.3]

if model == 0:
    corrmatrix = np.corrcoef([df1s.kteven,df1s.ktodd])
    covmatrix = np.cov([df1s.kteven,df1s.ktodd])
elif model == 1:
    corrmatrix = np.corrcoef([df1s.kV,df1s.kteven,df1s.ktodd])
    covmatrix = np.cov([df1s.kV,df1s.kteven,df1s.ktodd])
elif model == 2:
    corrmatrix = np.corrcoef([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kga])
    covmatrix = np.cov([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kga])
elif model == 3:
    corrmatrix = np.corrcoef([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kg,df1s.kga])
    covmatrix = np.cov([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kg,df1s.kga])
elif model == 4:
    corrmatrix = np.corrcoef([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kg,df1s.kga, df1s.kggZH])
    covmatrix = np.cov([df1s.kV,df1s.kteven,df1s.ktodd,df1s.kg,df1s.kga, df1s.kggZH])

else:
    print("Wrong model.")

# print("68% C.L. linear correlation matrix: ")
# print(corrmatrix)
#
# print("Covariance matrix: ")
# print(covmatrix)

df2s = df.loc[df.chi2tot <= c2t_min + 6.18]
df1s1D = df.loc[df.chi2tot <= c2t_min + 1.]
df2s1D = df.loc[df.chi2tot <= c2t_min + 4.]

if highlight_smlike_points:
    dfsmlike = df.loc[abs(df.kV-1.) <= 0.02]
    dfsmlike = dfsmlike.loc[abs(dfsmlike.kteven-1.) <= 0.02]
    print("Number of SM-like points: ", dfsmlike.size )

if projection:
    dfproj = df.loc[(df.mu_tH_over_ttHtWH >= mu_tH_over_ttHtWH_proj_lower)]
    dfproj = dfproj.loc[(df.mu_tH_over_ttHtWH <= mu_tH_over_ttHtWH_proj_upper)]
    print("Number of projection points: ", dfproj.size )
#df1s_BP = df1s.loc[(abs(df.kteven - 0.7) < 0.025) & (abs(df.ktodd - 1.0) < 0.025)]
# df1s_BP = df1s.loc[(abs(df.kteven - 1.0) < 0.025) & (abs(df.ktodd - 0.6) < 0.025)]

# print(df1s_BP[['kteven','ktodd','kV','kg','kga','chi2tot','mu_ttH_gaga','mu_ttH']])


def job_plotpairings(p):
	print("Creating figure "+output_dir+p[0]+p[1]+"_"+style+".pdf")
	fig,ax = plt.subplots(figsize=(5,4))
	par = [[],[]]
	parBF = [0, 0]

	if 'r_tH' in p:
		par[p.index('r_tH')] = list(map(lambda kteven,ktodd,kV: r_tH_func(kteven,ktodd,kV),df['kteven'],df['ktodd'],df['kV']))
		par[p.index('r_tH')] = list(map(lambda r: r if (r >= range_dict['r_tH'][0] and r <= range_dict['r_tH'][1]) else -1., par[p.index('r_tH')]))
		# print("r_tH: ", par[p.index('r_tH')][:10])
		# parBF[p.index('r_tH')] = r_tH_BF
	if 'r_ttH' in p:
		par[p.index('r_ttH')] = list(map(lambda kteven,ktodd: r_ttH(kteven,ktodd),df['kteven'],df['ktodd']))
		# parBF[p.index('r_ttH')] = r_ttH_BF
	if 'gamma' in p:
		par[p.index('gamma')] = list(map(lambda kteven,ktodd: np.arctan(ellipsecoeff*ktodd/kteven),df['kteven'],df['ktodd']))
		# parBF[p.index('r_ttH')] = r_ttH_BF
	if 'mu_ttHtH' in p:
		par[p.index('mu_ttHtH')] = list(map(lambda m1,kteven,ktodd,kV: (m1*s_ttH+r_tH_func(kteven,ktodd,kV)*s_tH)/(s_ttH+s_tH), df['mu_ttH'],df['kteven'],df['ktodd'],df['kV']))
	if 'r_tHttH' in p:
		par[p.index('r_tHttH')] = list(map(lambda kteven,ktodd,kV,m2: r_tH_func(kteven,ktodd,kV)/m2, df['kteven'],df['ktodd'],df['kV'],df['mu_ttH']))
		par[p.index('r_tHttH')] = list(map(lambda r: r if (r >= range_dict['r_tHttH'][0] and r <= range_dict['r_tHttH'][1]) else -1., par[p.index('r_tHttH')]))
	if 'xsr_tHttH' in p:
		par[p.index('xsr_tHttH')] = list(map(lambda kteven,ktodd,kV,m2: (r_tH_func(kteven,ktodd,kV)*s_tH/(m2*s_ttH))/(s_tH/s_ttH), df['kteven'],df['ktodd'],df['kV'],df['mu_ttH']))
		par[p.index('xsr_tHttH')] = list(map(lambda r: r if (r >= range_dict['xsr_tHttH'][0] and r <= range_dict['xsr_tHttH'][1]) else -1., par[p.index('xsr_tHttH')]))

	if highlight_smlike_points:
		#smlike_muttHtH = list(map(lambda m1,kteven,ktodd,kV: (m1*s_ttH+r_tH_func(kteven,ktodd,kV)*s_tH)/(s_ttH+s_tH), dfsmlike['mu_ttH'],dfsmlike['kteven'],dfsmlike['ktodd'],dfsmlike['kV']))
		smlike_muttHtH = dfsmlike['mu_topH']
		#smlike_rtHttH = list(map(lambda kteven,ktodd,kV,m2: r_tH_func(kteven,ktodd,kV)/m2, dfsmlike['kteven'],dfsmlike['ktodd'],dfsmlike['kV'],dfsmlike['mu_ttH']))
		smlike_rtHttH = dfsmlike['mu_tH_over_ttHtWH']

	if p[0] not in ['r_tH','r_ttH','gamma','mu_ttHtH','r_tHttH','xsr_tHttH']:
		par[0]=df[p[0]]
		# parBF[0] = par[0][minindex]
	if p[1] not in ['r_tH','r_ttH','gamma','mu_ttHtH','r_tHttH','xsr_tHttH']:
		par[1]=df[p[1]]
		# parBF[1] = par[1][minindex]

	if style=="scatter":
		# DOES NOT WORK FOR DERIVED QUANTITIES (r_ttH, r_tH)
		ax.plot(df[p[0]],df[p[1]],'o',color='gray',rasterized=True)
		ax.plot(df2s[p[0]],df2s[p[1]],'o',color='orange',rasterized=True)
		ax.plot(df1s[p[0]],df1s[p[1]],'o',color='red',rasterized=True)
	elif style=="hexbins":
		hb = ax.hexbin(par[0],par[1],Deltac2t,reduce_C_function = min,vmax=20,cmap='magma',gridsize=hexbins)
		cb = fig.colorbar(hb, ax=ax, pad =0.02, shrink=0.9)
		cb.ax.set_title(r'$\Delta\chi^2$',pad=15,loc='left',fontsize=fs)
		cb.ax.plot([0, 20], [2.3,2.3], linestyle = '--', color = '#ffffff')
		cb.ax.plot([0, 20], [6.18,6.18], linestyle = '--', color = '#cccccc')
		cb.ax.plot([0, 20], [11.83,11.83], linestyle = '--', color = '#444444')
	if add_contours:
		profile2d = stats.binned_statistic_2d(par[1],par[0],Deltac2t,statistic='min',bins=contourbins)
		# print(profile2d.x_edge)
		# print(profile2d.y_edge)
		# print(profile2d.statistic)
		if shift_contour_bins:
			x_bin_interval = np.abs(profile2d.x_edge[1]-profile2d.x_edge[0])
			y_bin_interval = np.abs(profile2d.y_edge[1]-profile2d.y_edge[0])
			xbinvals = list(map(lambda x: x+ x_bin_interval/2., profile2d.x_edge[:-1]))
			ybinvals = list(map(lambda y: y+ y_bin_interval/2., profile2d.y_edge[:-1]))
		else:
			xbinvals = profile2d.x_edge[:-1]
			ybinvals = profile2d.y_edge[:-1]
		if style =='plain':
			hb = ax.hexbin(par[0],par[1],[20. for d in Deltac2t],reduce_C_function = max,vmin=0,vmax=20,cmap='magma',gridsize=hexbins)
			cs1 = ax.contour(ybinvals, xbinvals , profile2d.statistic,
					 linestyles='--', colors=['#222222','#999999','#cccccc'], levels=[2.30, 6.18, 11.83])
			fmt = {}
			strs = [r'$1\sigma$', r'$2\sigma$', r'$3\sigma$']
			for l, s in zip(cs1.levels, strs):
			    fmt[l] = s
			if p[0] == 'kteven':
				pos = [(0.7,2.2),(0.4,3.7),(0.05,4.6)]
			if p[0] == 'ktodd':
				pos = [(0.6,2.1),(1.,6.),(0.2,3.2)]
			ax.clabel(cs1, cs1.levels, inline=True, fmt=fmt, fontsize=12, manual=pos)
		else:
			ax.contour(ybinvals, xbinvals , profile2d.statistic,
					 linestyles='--', colors=['#ffffff','#cccccc','#444444'], levels=[2.30, 6.18, 11.83])
	if (highlight_smlike_points and ( p[1] == 'mu_topH' or p[1] == 'mu_tH_over_ttHtWH')) :
		yvals = []
		if p[1] == 'mu_topH':
			yvals = smlike_muttHtH
		if p[1] == 'mu_tH_over_ttHtWH':
			yvals = smlike_rtHttH

		print("Drawing SM-like points... ")
		shift = (max(dfsmlike[p[0]]) - min(dfsmlike[p[0]]))/(2*50.)
		binnedprofile_min, binedges_min, binnumber_min = stats.binned_statistic(dfsmlike[p[0]],yvals,statistic='min',bins = 50)
		binnedprofile_max, binedges_max, binnumber_max = stats.binned_statistic(dfsmlike[p[0]],yvals,statistic='max',bins = 50)
		if shiftbins:
		    xvals = list(map(lambda x: x+shift, binedges_min[:-1]))
                # xvals_max = list(map(lambda x: x+shift, binedges_max[:-1]))
		else:
		    xvals = binedges_min[:-1]
                # xvals_max = binedges_max[:-1]

		ax.fill_between(xvals,binnedprofile_min,binnedprofile_max,facecolor='green', alpha=0.5)#,color=lc[m])

		if p[1] == 'mu_topH':
			ax.text(0.8,0.665,r'$c_t, c_V \approx 1$', transform=ax.transAxes, fontsize = 8, rotation=45, color='k')
		if p[1] == 'mu_tH_over_ttHtWH':
			ax.text(0.8,0.28,r'$c_t, c_V \approx 1$', transform=ax.transAxes, fontsize = 8, rotation=12, color='k')

	if (projection and p[0] == 'kteven' and p[1] == 'ktodd'):
		shift = (max(dfproj[p[0]]) - min(dfproj[p[0]]))/(2*50.)
		binnedprofile_min, binedges_min, binnumber_min = stats.binned_statistic(dfproj[p[0]],dfproj[p[1]],statistic='min',bins = 50)
		binnedprofile_max, binedges_max, binnumber_max = stats.binned_statistic(dfproj[p[0]],dfproj[p[1]],statistic='max',bins = 50)
		if shiftbins:
			xvals = list(map(lambda x: x+shift, binedges_min[:-1]))
		else:
	            	xvals = binedges_min[:-1]

		ax.fill_between(xvals,binnedprofile_min,binnedprofile_max,facecolor='blue', alpha=0.2)#,color=lc[m])
		ax.text(0.50,0.70,r'$tH,H\to \gamma \gamma~w/~3~\mathrm{ab}^{-1}$', transform=ax.transAxes, fontsize = 12, rotation=0, color='b')

	if (projection and p[1] == 'mu_tH_over_ttHtWH'):
		xvals = np.arange(range_dict[p[0]][0],range_dict[p[0]][1],(range_dict[p[0]][1]-range_dict[p[0]][0])/100.)
		yvals_upper = [mu_tH_over_ttHtWH_proj_upper for x in xvals]
		yvals_lower = [mu_tH_over_ttHtWH_proj_lower for x in xvals]
		ax.fill_between(xvals, yvals_lower, yvals_upper, facecolor='blue', alpha = 0.20)
		if(p[0]=='kteven'):
			ax.text(0.05,0.08,r'$tH,H\to \gamma \gamma~w/~3~\mathrm{ab}^{-1}$', transform=ax.transAxes, fontsize = 12, rotation=0, color='b')
		if(p[0]=='ktodd'):
			ax.text(0.05,0.08,r'$tH,H\to \gamma \gamma~w/~3~\mathrm{ab}^{-1}$', transform=ax.transAxes, fontsize = 12 , rotation=0, color='b')

			# ax.plot(dfsmlike[p[0]],smlike_muttHtH,'o',color='red')
	if draw_SM:
                ax.plot(SM_vals[p[0]],SM_vals[p[1]],'X',markersize=8,color='orange')
	if draw_BF:
                ax.plot(BF_vals[p[0]],BF_vals[p[1]],'*',markersize=10,color='w')
	ax.set_xlim(range_dict[p[0]])
	ax.set_ylim(range_dict[p[1]])
	ax.set_xlabel(label_dict[p[0]])
	ax.set_ylabel(label_dict[p[1]])
	ax.tick_params(direction='in',which='major',length=6,width=1,grid_alpha=0.5)
	ax.tick_params(direction='in',which='minor',length=3,width=1)
	if p[0] in tick_dict.keys():
		ax.xaxis.set_major_locator(MultipleLocator(tick_dict[p[0]][0]))
		ax.xaxis.set_minor_locator(AutoMinorLocator(tick_dict[p[0]][1]))
	if p[1] in tick_dict.keys():
		ax.yaxis.set_major_locator(MultipleLocator(tick_dict[p[1]][0]))
		ax.yaxis.set_minor_locator(AutoMinorLocator(tick_dict[p[1]][1]))
	plt.grid()
	ax.text(0.02,1.02,modellabel,transform=ax.transAxes)
	ax.text(0.02,0.94,addlabel,fontsize=14,color='g',transform=ax.transAxes)

	fig.savefig(output_dir+p[0]+p[1]+"_"+style+label+".pdf",bbox_inches='tight')

def job_chi2profiles(p):
    print("Creating figure "+output_dir+p+"_1D.pdf")
    fig,ax = plt.subplots(figsize=(5,5))
    par = []
    par1s = []
    par2s = []
    if(p == 'cosphi'):
        par = list(map(lambda kt, kttilde: kt/np.sqrt(kt**2.+kttilde**2), df['kteven'],df['ktodd']))
        par1s = list(map(lambda kt, kttilde: kt/np.sqrt(kt**2.+kttilde**2), df1s1D['kteven'],df1s1D['ktodd']))
        par2s = list(map(lambda kt, kttilde: kt/np.sqrt(kt**2.+kttilde**2), df2s1D['kteven'],df2s1D['ktodd']))
    elif p == 'phi':
        par = list(map(lambda kt, kttilde: np.arcsin(kttilde/np.sqrt(kt**2.+kttilde**2)), df['kteven'],df['ktodd']))
        par1s = list(map(lambda kt, kttilde: np.arcsin(kttilde/np.sqrt(kt**2.+kttilde**2)), df1s1D['kteven'],df1s1D['ktodd']))
        par2s = list(map(lambda kt, kttilde: np.arcsin(kttilde/np.sqrt(kt**2.+kttilde**2)), df2s1D['kteven'],df2s1D['ktodd']))
    else:
        par = df[p]
        par1s = df1s1D[p]
        par2s = df2s1D[p]
    ax.plot(par,Deltac2t,'.',color='lightgrey', zorder = 1, rasterized=True)
    if draw_BF:
        ax.axvline(BF_vals[p],linewidth=2,color='grey' ,zorder = 5)
    print(p, min(par2s), max(par2s))
    ax.fill_betweenx([0,20.],[min(par2s),min(par2s)],[max(par2s),max(par2s)],color='yellow',alpha=0.3,zorder=3)
    ax.fill_betweenx([0,20.],[min(par1s),min(par1s)],[max(par1s),max(par1s)],color='green',alpha=0.3,zorder=3)
    ax.set_xlim(range_dict[p])
    ax.set_ylim([0.,20.])
    ax.set_xlabel(label_dict[p])
    ax.set_ylabel(r"$\Delta\chi^2$")
    ax.tick_params(direction='in',which='major',length=6,width=1,grid_alpha=0.5,pad = padsize)
    ax.tick_params(direction='in',which='minor',length=3,width=1,pad = padsize)
    if p in tick_dict.keys():
        ax.xaxis.set_major_locator(MultipleLocator(tick_dict[p][0]))
        ax.xaxis.set_minor_locator(AutoMinorLocator(tick_dict[p][1]))
    # if p[1] in tick_dict.keys():
    ax.yaxis.set_major_locator(MultipleLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))
    ax.text(0.02,1.02,modellabel,fontsize=fs,transform=ax.transAxes)
    ax.text(0.02,0.94,addlabel,fontsize=fs2,transform=ax.transAxes)

    plt.grid(zorder=20)
    fig.savefig(output_dir+p+"_1D"+label+".pdf",bbox_inches='tight')

if __name__ == '__main__':

	pool = mp.Pool()

	pool.map(job_plotpairings, plotpairings)
#	pool.map(job_chi2profiles, chi2profiles)
